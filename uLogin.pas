unit uLogin;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls,
  Vcl.Imaging.pngimage;

type
  TFrmLogin = class(TForm)
    Image1: TImage;
    EdtLogin: TEdit;
    EdtSenha: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Panel1: TPanel;
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Panel1Click(Sender: TObject);
    procedure EdtLoginKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdtSenhaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public

    Procedure Entrar;
    { Public declarations }
  end;

var
  FrmLogin: TFrmLogin;

implementation

{$R *.dfm}

uses uDM, uFuncoes;

{ TFrmLogin }

procedure TFrmLogin.EdtLoginKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_RETURN then
  begin
    EdtSenha.SetFocus;
  end;
end;

procedure TFrmLogin.EdtSenhaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_RETURN then
  begin
    Entrar;
  end;
end;

procedure TFrmLogin.Entrar;
begin
  DM.CDSLogin.Close;
  DM.CDSLogin.ParamByName('LOGIN').Value := edtLogin.Text;
  DM.CDSLogin.ParamByName('SENHA').Value := EdtSenha.Text;
  DM.CDSLogin.Open;

  if DM.CDSLogin.RecordCount > 0 then
  begin
    DM.ID_USUARIO   := DM.CDSLoginID_USUARIO.Value;
    DM.NOME_USUARIO := DM.CDSLoginNOME.Value;
    Close;
  end
  else
  begin
    ShowMessage('No foi possivel Logar, Verifique usuario ou senha.');
    edtLogin.SetFocus;
    Exit;
  end;

end;

procedure TFrmLogin.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 case Key  of

  VK_ESCAPE: Application.Terminate;
  end;

end;

procedure TFrmLogin.Panel1Click(Sender: TObject);
begin
  Entrar;
end;

end.
