unit uParcelas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls, Data.DB,
  Vcl.Grids, Vcl.DBGrids, Vcl.WinXCtrls, Vcl.Imaging.pngimage, frxClass,
  frxDBSet, Vcl.ComCtrls;

type
  TfrmParcelas = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Splitter2: TSplitter;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    DBGrid2: TDBGrid;
    GroupBox2: TGroupBox;
    Label2: TLabel;
    CBFiltro: TComboBox;
    SBPesquisar: TSearchBox;
    CBMesCadastro: TComboBox;
    DSConsParcelas: TDataSource;
    Panel4: TPanel;
    DSConsClienteParcela: TDataSource;
    DBGrid1: TDBGrid;
    BtnBaixar: TImage;
    Panel5: TPanel;
    BtnGerar: TImage;
    BtnImprimir: TImage;
    frxDBRecibo: TfrxDBDataset;
    frxRRecibo: TfrxReport;
    DSRecibo: TDataSource;
    DT2: TDateTimePicker;
    DT1: TDateTimePicker;
    CBFiltroParcela: TComboBox;
    Label1: TLabel;
    Label3: TLabel;
    Shape1: TShape;
    Label4: TLabel;
    Shape2: TShape;
    Label5: TLabel;
    Btncaixa: TImage;
    procedure SBPesquisarInvokeSearch(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure DBGrid2DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure BtnBaixarClick(Sender: TObject);
    procedure BtnGerarClick(Sender: TObject);
    procedure BtnImprimirClick(Sender: TObject);
    procedure DT1CloseUp(Sender: TObject);
    procedure DT2CloseUp(Sender: TObject);
    procedure CBFiltroParcelaCloseUp(Sender: TObject);
    procedure DBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBFiltroClick(Sender: TObject);
    procedure CBMesCadastroClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure BtncaixaClick(Sender: TObject);
    procedure CBFiltroParcelaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGrid2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
  Procedure PesquisaParcelas;
  public
    { Public declarations }
  end;

var
  frmParcelas: TfrmParcelas;

implementation

{$R *.dfm}

uses uDM, uFuncoes, uBaixarParcela, uRelParcelasPagas;

procedure TfrmParcelas.CBFiltroClick(Sender: TObject);
begin
    if CBFiltro.ItemIndex = 10 then
    begin
      SBPesquisar.Visible := False;
      CBMesCadastro.Visible := True;
    end
    else
    begin
     SBPesquisar.Visible := True;
     CBMesCadastro.Visible := False;
    end;

end;

procedure TfrmParcelas.CBFiltroParcelaCloseUp(Sender: TObject);
begin
 PesquisaParcelas;
end;

procedure TfrmParcelas.CBFiltroParcelaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
   if Key = VK_DELETE then
   begin
     CBFiltroParcela.ItemIndex := -1;
     CBFiltro.Text := '';

     DM.CDSConsParcelas.Filtered := False;
     DM.CDSRecibo.Filtered := False;
   end;
end;

procedure TfrmParcelas.CBMesCadastroClick(Sender: TObject);
var Filtro1, Filtro2 : String;
begin
     DM.CDSConsClienteParcela.Close;
     DM.CDSConsClienteParcela.Open;

     DM.CDSConsClienteParcela.Filtered := False;
     DM.CDSConsClienteParcela.Filter   := 'Month(DATA_INSTALACAO) = ' +  IntToStr(CBMesCadastro.ItemIndex + 1) ;
     DM.CDSConsClienteParcela.Filtered := True;

     if DM.CDSConsClienteParcela.RecordCount = 0 then
     begin
        ShowMessage('N�o foi possivel encontrar nada com essa pesquisa');
        BtnGerar.Visible := False;
     end
     else
     begin
       BtnGerar.Visible := True;
       DBGrid2.SetFocus;
     end;
end;

procedure TfrmParcelas.DBGrid1CellClick(Column: TColumn);
begin
  DM.CDSConsParcelas.Close;
  DM.CDSConsParcelas.ParamByName('ID_CLIENTE').Value := DM.CDSConsClienteParcelaID_CLIENTE.Value;
  DM.CDSConsParcelas.Open;

  if DM.CDSConsParcelas.RecordCount > 0 then
  begin
    BtnImprimir.Visible := True;
    BtnBaixar.Visible := True;
  end
  else
  begin
    BtnImprimir.Visible := False;
    BtnBaixar.Visible := False;
    BtnGerar.Visible := True;
  end;
end;

procedure TfrmParcelas.DBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if KEY = VK_RETURN then
  begin
    DM.CDSConsParcelas.Close;
    DM.CDSConsParcelas.ParamByName('ID_CLIENTE').Value := DM.CDSConsClienteParcelaID_CLIENTE.Value;
    DM.CDSConsParcelas.Open;

    if DM.CDSConsParcelas.RecordCount > 0 then
    begin
      BtnImprimir.Visible := True;
      BtnBaixar.Visible := True;
    end
    else
    begin
      BtnImprimir.Visible := False;
      BtnBaixar.Visible := False;
      BtnGerar.Visible := True;
    end;

  end;
end;

procedure TfrmParcelas.DBGrid2DrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
const
clPaleGreen = TColor($98FB98);
clPaleRed = TColor($7280FA);
clPaleYellow = TColor($00FFFF);
clPaleBlue = TColor($E6D8AD);
clPaleTeal = TColor($808000);
clBlue = TColor($FF0000);
clGray = TColor ($A9A9A9);
begin

   //PAGO
  if Column.Field.Dataset.FieldbyName('STATUS').AsString = 'P'
   then
   begin
       If (gdFocused in State)
	     then
     else DBGrid2.Canvas.brush.color := clPaleGreen;
              DBGrid2.DefaultDrawColumnCell(rect,DataCol,Column,State);
   end;


  //VENCIDA
  if Column.Field.Dataset.FieldbyName('STATUS').AsString = 'V'
   then
   begin
       If (gdFocused in State)
	     then
     else DBGrid2.Canvas.brush.color := clPaleRed;
              DBGrid2.DefaultDrawColumnCell(rect,DataCol,Column,State);
   end;

end;

procedure TfrmParcelas.DBGrid2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if Key = VK_DELETE then
 begin
   if MessageBox(0, 'Deseja Excluir a Parcela?', '', MB_ICONQUESTION or MB_YESNO) = mrYes  then
   begin
    DM.CDSEditParcela.Close;
    DM.CDSEditParcela.ParamByName('ID_PARCELA').Value := DM.CDSConsParcelasID_PARCELA.Value;
    DM.CDSEditParcela.Open;
    DM.CDSEditParcela.Edit;

    DM.CDSEditParcela.Delete;
    DM.CDSEditParcela.ApplyUpdates(0);

    DM.CDSConsParcelas.Close;
    DM.CDSConsParcelas.Open;
   end;
  end;
end;

procedure TfrmParcelas.DT1CloseUp(Sender: TObject);
begin
 PesquisaParcelas;
end;

procedure TfrmParcelas.DT2CloseUp(Sender: TObject);
begin
PesquisaParcelas;
end;

procedure TfrmParcelas.FormCreate(Sender: TObject);
begin
  uFuncoes.DimensionarGrid(DBGrid1);
  uFuncoes.DimensionarGrid(DBGrid2);

  DT1.Date := Now;
  DT2.Date := Now;

  DM.CDSConsClienteParcela.Close;
  DM.CDSConsClienteParcela.Open;

  DM.CDSConsParcelas.Close;
  DM.CDSConsParcelas.Open;

end;

procedure TfrmParcelas.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_ESCAPE then
  Close;

end;

procedure TfrmParcelas.FormResize(Sender: TObject);
begin
  uFuncoes.DimensionarGrid(DBGrid1);
  uFuncoes.DimensionarGrid(DBGrid2);
end;

procedure TfrmParcelas.BtnBaixarClick(Sender: TObject);
begin
  try
    Application.CreateForm(TfrmBaixarParcela, frmBaixarParcela);

    DM.CDSEditParcela.Close;
    DM.CDSEditParcela.ParamByName('ID_PARCELA').Value := DM.CDSConsParcelasID_PARCELA.Value;
    DM.CDSEditParcela.Open;
    DM.CDSEditParcela.Edit;

    if DM.CDSEditParcela.RecordCount > 0 then
    begin
      frmBaixarParcela.lbNomeCliente.Caption := DM.CDSConsClienteParcelaNOME.Value;
      frmBaixarParcela.lbNParcela.Caption := 'N� Parcela: ' + DM.CDSConsParcelasNUM_PARCELA.AsString;
      frmBaixarParcela.lbVParcela.Caption := 'Valor Parcela: R$ ' + DM.CDSConsParcelasVALOR.AsString;
    end;

    frmBaixarParcela.ShowModal;
  finally
    frmBaixarParcela.Free;
  end;
end;

procedure TfrmParcelas.BtncaixaClick(Sender: TObject);
begin
   try
     Application.CreateForm(TFrmRelParcelasPagas, FrmRelParcelasPagas);
     FrmRelParcelasPagas.ShowModal;
   finally
     FrmRelParcelasPagas.Free;
   end;
end;

procedure TfrmParcelas.BtnGerarClick(Sender: TObject);
var I : Integer;
DataVencimento, DataUltimaParcela, DataConv : TDateTime;
Dia, Mes, Ano, AnoAtual, MesAtual, DiaAtual : Word;
begin
  DM.CDSConsClienteParcela.First;

  if DM.CDSConsClienteParcela.RecordCount > 1 then
  begin
      if MessageBox(0, 'Deseja Gerar Parcela para TODOS os clientes filtrados?', '', MB_ICONQUESTION or MB_YESNO) = mrYes  then
      begin

        while not DM.CDSConsClienteParcela.Eof do
        begin

         DM.CDSCadParcela.Close;
         DM.CDSCadParcela.ParamByName('ID_CLIENTE').Value := DM.CDSConsClienteParcelaID_CLIENTE.Value;
         DM.CDSCadParcela.Open;
         DM.CDSCadParcela.Last;

         DM.CDSConfig.Close;
         DM.CDSConfig.Open;

         if DM.CDSCadParcelaDATA_VENCIMENTO.Value = StrToDate('30/12/1899') then
            DataUltimaParcela := Now
         else
            DataUltimaParcela := DM.CDSCadParcelaDATA_VENCIMENTO.Value;        
       

         DataVencimento := DataUltimaParcela;
         DecodeDate(DataVencimento, Ano, Mes, Dia);
         DecodeDate(Now, AnoAtual, MesAtual, DiaAtual);

         //Gera 12 Parcelas
         for I := 1 to 12 do
         begin

           if (MesAtual = Mes) and (Dia < 15)  then
           begin
             DataVencimento := DataVencimento + (15 - Dia);
             DecodeDate(DataVencimento, Ano, Mes, Dia);
             DataConv  := EncodeDate(Ano, Mes, 15);
           end
           else
           begin
            DataVencimento := DataVencimento + 30;
            DecodeDate(DataVencimento, Ano, Mes, Dia);
            DataConv  := EncodeDate(Ano, Mes, 15);
           end;


           DM.CDSCadParcela.Append;
           DM.CDSCadParcelaID_PARCELA.Value := uFuncoes.RetornaId('ID_PARCELA');
           DM.CDSCadParcelaID_CLIENTE.Value := DM.CDSConsClienteParcelaID_CLIENTE.Value;
           DM.CDSCadParcelaVALOR.Value      := DM.CDSConfigVALOR_MENSALIDADE.Value;
           DM.CDSCadParcelaNUM_PARCELA.Value:= I;
           DM.CDSCadParcelaDATA_VENCIMENTO.Value := DataConv;
           DM.CDSCadParcelaSTATUS.Value     := 'A';

           DM.CDSCadParcela.Post;
           DM.CDSCadParcela.ApplyUpdates(0);
         end; 

        DM.CDSConsClienteParcela.Next;
        end;

        
        ShowMessage('Parcelas Geradas com Sucesso!');
      end
      else
      begin
        Exit;
      end;
  end
  else
  begin
       //Gerando Parcela para Cliente Unico.
       DM.CDSCadParcela.Close;
       DM.CDSCadParcela.ParamByName('ID_CLIENTE').Value := DM.CDSConsClienteParcelaID_CLIENTE.Value;
       DM.CDSCadParcela.Open;
       DM.CDSCadParcela.Last;

       DM.CDSConfig.Close;
       DM.CDSConfig.Open;

       if DM.CDSCadParcelaDATA_VENCIMENTO.Value = StrToDate('30/12/1899') then
          DataUltimaParcela := Now
       else
          DataUltimaParcela := DM.CDSCadParcelaDATA_VENCIMENTO.Value;        
       

       DataVencimento := DataUltimaParcela;
       DecodeDate(DataVencimento, Ano, Mes, Dia);
       DecodeDate(Now, AnoAtual, MesAtual, DiaAtual);

       //Gera 12 Parcelas
       for I := 1 to 12 do
       begin

         if (MesAtual = Mes) and (Dia < 15)  then
         begin
           DataVencimento := DataVencimento + (15 - Dia);
           DecodeDate(DataVencimento, Ano, Mes, Dia);
           DataConv  := EncodeDate(Ano, Mes, 15);
         end
         else
         begin
          DataVencimento := DataVencimento + 30;
          DecodeDate(DataVencimento, Ano, Mes, Dia);
          DataConv  := EncodeDate(Ano, Mes, 15);
         end;


         DM.CDSCadParcela.Append;
         DM.CDSCadParcelaID_PARCELA.Value := uFuncoes.RetornaId('ID_PARCELA');
         DM.CDSCadParcelaID_CLIENTE.Value := DM.CDSConsClienteParcelaID_CLIENTE.Value;
         DM.CDSCadParcelaVALOR.Value      := DM.CDSConfigVALOR_MENSALIDADE.Value;
         DM.CDSCadParcelaNUM_PARCELA.Value:= I;
         DM.CDSCadParcelaDATA_VENCIMENTO.Value := DataConv;
         DM.CDSCadParcelaSTATUS.Value     := 'A';

         DM.CDSCadParcela.Post;
       end;

       DM.CDSCadParcela.ApplyUpdates(0);
       ShowMessage('Parcelas Geradas com Sucesso!');
  end;
end;

procedure TfrmParcelas.BtnImprimirClick(Sender: TObject);
begin
  if DM.CDSConsClienteParcela.RecordCount > 0 then
  begin
    DM.CDSRecibo.Close;
    DM.CDSRecibo.ParamByName('ID_CLIENTE').Value := DM.CDSConsClienteParcelaID_CLIENTE.Value;
    DM.CDSRecibo.Open;

    frxRRecibo.PrepareReport(True);
    frxRRecibo.ShowReport();
  end;
end;

procedure TfrmParcelas.PesquisaParcelas;
var Filtro : String;
begin
  DM.CDSConsParcelas.Close;
  DM.CDSConsParcelas.ParamByName('ID_CLIENTE').Value := DM.CDSConsClienteParcelaID_CLIENTE.Value;
  DM.CDSConsParcelas.Open;

  DM.CDSRecibo.Close;
  DM.CDSRecibo.ParamByName('ID_CLIENTE').Value := DM.CDSConsClienteParcelaID_CLIENTE.Value;
  DM.CDSRecibo.Open;


     {
    Abertas
    Pagas
    Vencidas
    }

    case CBFiltroParcela.ItemIndex of
    0:
     begin
       Filtro := 'A';
     end;

    1:
     begin
       Filtro := 'P';
     end;


    2:
     begin
       Filtro := 'V';
     end;

     else
     begin
       Filtro := '%';
     end;
    end;

    if (DT1.Date = Date) and (DT2.Date = Date) then
    begin
      DM.CDSConsParcelas.Filtered := False;
      DM.CDSConsParcelas.Filter   := 'STATUS=' + QuotedStr(Filtro) ;
      DM.CDSConsParcelas.Filtered := True;

      DM.CDSRecibo.Filtered := False;
      DM.CDSRecibo.Filter   := 'STATUS=' + QuotedStr(Filtro) ;
      DM.CDSRecibo.Filtered := True;
    end
    else
    begin
      DM.CDSConsParcelas.Filtered := False;
      DM.CDSConsParcelas.Filter := 'DATA_VENCIMENTO >= ' + QuotedStr(DateToStr(DT1.Date)) + 'AND DATA_VENCIMENTO <= ' + QuotedStr(DateToStr(DT2.Date)) + ' AND STATUS LIKE' + QuotedStr(Filtro) ;
      DM.CDSConsParcelas.Filtered := True;


      DM.CDSRecibo.Filtered := False;
      DM.CDSRecibo.Filter := 'DATA_VENCIMENTO >= ' + QuotedStr(DateToStr(DT1.Date)) + 'AND DATA_VENCIMENTO <= ' + QuotedStr(DateToStr(DT2.Date)) + ' AND STATUS LIKE' + QuotedStr(Filtro) ;
      DM.CDSRecibo.Filtered := True;
    end;


end;

procedure TfrmParcelas.SBPesquisarInvokeSearch(Sender: TObject);
begin
  {
  Codigo
  Nome
  Endere�o
  Bairro
  Telefone
  Celular
  Status
  Vencimento
  CPF
  RG
  }


  case CBFiltro.ItemIndex of

    0:
    begin
     DM.CDSConsClienteParcela.Close;
     DM.CDSConsClienteParcela.Open;

     DM.CDSConsClienteParcela.Filtered := False;
     DM.CDSConsClienteParcela.Filter   := 'ID_CLIENTE='  + SBPesquisar.Text;
     DM.CDSConsClienteParcela.Filtered := True;

     if DM.CDSConsClienteParcela.RecordCount = 0 then
     begin
        ShowMessage('N�o foi possivel encontrar nada com essa pesquisa');
        BtnGerar.Visible := False;
     end
     else
     begin
       BtnGerar.Visible := True;
       DBGrid2.SetFocus;
     end;

    end;


   1:
    begin
     DM.CDSConsClienteParcela.Close;
     DM.CDSConsClienteParcela.Open;

     DM.CDSConsClienteParcela.Filtered := False;
     DM.CDSConsClienteParcela.Filter   := 'NOME LIKE '  + QuotedStr(SBPesquisar.Text + '%');
     DM.CDSConsClienteParcela.Filtered := True;

     if DM.CDSConsClienteParcela.RecordCount = 0 then
     begin
        ShowMessage('N�o foi possivel encontrar nada com essa pesquisa');
        BtnGerar.Visible := False;
     end
     else
     begin
       BtnGerar.Visible := True;
       DBGrid2.SetFocus;
     end;
    end;

    2:
    begin
     DM.CDSConsClienteParcela.Close;
     DM.CDSConsClienteParcela.Open;

     DM.CDSConsClienteParcela.Filtered := False;
     DM.CDSConsClienteParcela.Filter   := 'ENDERECO LIKE '  + QuotedStr(SBPesquisar.Text + '%');
     DM.CDSConsClienteParcela.Filtered := True;

     if DM.CDSConsClienteParcela.RecordCount = 0 then
     begin
        ShowMessage('N�o foi possivel encontrar nada com essa pesquisa');
        BtnGerar.Visible := False;
     end
     else
     begin
       BtnGerar.Visible := True;
       DBGrid2.SetFocus;
     end;
    end;

    3:
    begin
     DM.CDSConsClienteParcela.Close;
     DM.CDSConsClienteParcela.Open;

     DM.CDSConsClienteParcela.Filtered := False;
     DM.CDSConsClienteParcela.Filter   := 'BAIRRO LIKE '  + QuotedStr(SBPesquisar.Text + '%');
     DM.CDSConsClienteParcela.Filtered := True;

     if DM.CDSConsClienteParcela.RecordCount = 0 then
     begin
        ShowMessage('N�o foi possivel encontrar nada com essa pesquisa');
        BtnGerar.Visible := False;
     end
     else
     begin
       BtnGerar.Visible := True;
       DBGrid2.SetFocus;
     end;
    end;


    4:
    begin
     DM.CDSConsClienteParcela.Close;
     DM.CDSConsClienteParcela.Open;

     DM.CDSConsClienteParcela.Filtered := False;
     DM.CDSConsClienteParcela.Filter   := 'TELEFONE LIKE '  + QuotedStr(SBPesquisar.Text + '%');
     DM.CDSConsClienteParcela.Filtered := True;

     if DM.CDSConsClienteParcela.RecordCount = 0 then
     begin
        ShowMessage('N�o foi possivel encontrar nada com essa pesquisa');
        BtnGerar.Visible := False;
     end
     else
     begin
       BtnGerar.Visible := True;
       DBGrid2.SetFocus;
     end;
    end;


    5:
    begin
     DM.CDSConsClienteParcela.Close;
     DM.CDSConsClienteParcela.Open;

     DM.CDSConsClienteParcela.Filtered := False;
     DM.CDSConsClienteParcela.Filter   := 'CELULAR LIKE '  + QuotedStr(SBPesquisar.Text + '%');
     DM.CDSConsClienteParcela.Filtered := True;

     if DM.CDSConsClienteParcela.RecordCount = 0 then
     begin
        ShowMessage('N�o foi possivel encontrar nada com essa pesquisa');
        BtnGerar.Visible := False;
     end
     else
     begin
       BtnGerar.Visible := True;
       DBGrid2.SetFocus;
     end;
    end;

    6:
    begin
     DM.CDSConsClienteParcela.Close;
     DM.CDSConsClienteParcela.Open;

     DM.CDSConsClienteParcela.Filtered := False;
     DM.CDSConsClienteParcela.Filter   := 'STATUS='  + QuotedStr(SBPesquisar.Text);
     DM.CDSConsClienteParcela.Filtered := True;

     if DM.CDSConsClienteParcela.RecordCount = 0 then
     begin
        ShowMessage('N�o foi possivel encontrar nada com essa pesquisa');
        BtnGerar.Visible := False;
     end
     else
     begin
       BtnGerar.Visible := True;
       DBGrid2.SetFocus;
     end;
    end;

    7:
    begin
     DM.CDSConsClienteParcela.Close;
     DM.CDSConsClienteParcela.Open;

     DM.CDSConsClienteParcela.Filtered := False;
     DM.CDSConsClienteParcela.Filter   := 'VENCIMENTO='  + QuotedStr(SBPesquisar.Text);
     DM.CDSConsClienteParcela.Filtered := True;

     if DM.CDSConsClienteParcela.RecordCount = 0 then
     begin
        ShowMessage('N�o foi possivel encontrar nada com essa pesquisa');
        BtnGerar.Visible := False;
     end
     else
     begin
       BtnGerar.Visible := True;
       DBGrid2.SetFocus;
     end;
    end;

    8:
    begin
     DM.CDSConsClienteParcela.Close;
     DM.CDSConsClienteParcela.Open;

     DM.CDSConsClienteParcela.Filtered := False;
     DM.CDSConsClienteParcela.Filter   := 'CPF_CNPJ='  + QuotedStr(SBPesquisar.Text);
     DM.CDSConsClienteParcela.Filtered := True;

     if DM.CDSConsClienteParcela.RecordCount = 0 then
     begin
        ShowMessage('N�o foi possivel encontrar nada com essa pesquisa');
        BtnGerar.Visible := False;
     end
     else
     begin
       BtnGerar.Visible := True;
       DBGrid2.SetFocus;
     end;
    end;


    9:
    begin
     DM.CDSConsClienteParcela.Close;
     DM.CDSConsClienteParcela.Open;

     DM.CDSConsClienteParcela.Filtered := False;
     DM.CDSConsClienteParcela.Filter   := 'RG_IE='  + QuotedStr(SBPesquisar.Text);
     DM.CDSConsClienteParcela.Filtered := True;

     if DM.CDSConsClienteParcela.RecordCount = 0 then
     begin
        ShowMessage('N�o foi possivel encontrar nada com essa pesquisa');
        BtnGerar.Visible := False;
     end
     else
     begin
       BtnGerar.Visible := True;
       DBGrid2.SetFocus;
     end;
    end;

    else
    begin
       ShowMessage('Selecione o Filtro antes da Pesquisa.');
    end;
  end;


  SBPesquisar.Text := '';
end;

end.
