unit uConsTecnicos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.Imaging.pngimage,
  Vcl.ExtCtrls, Vcl.Grids, Vcl.DBGrids;

type
  TFrmConsTecnicos = class(TForm)
    DBGrid1: TDBGrid;
    Panel1: TPanel;
    Image1: TImage;
    Image2: TImage;
    DSConsTecnicos: TDataSource;
    procedure Image1Click(Sender: TObject);
    procedure Image2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmConsTecnicos: TFrmConsTecnicos;

implementation

{$R *.dfm}

uses uDM, uFuncoes, uCadUsuarios, uCadTecnicos;

procedure TFrmConsTecnicos.FormCreate(Sender: TObject);
begin
  DM.CDSConsTecnicos.Close;
  DM.CDSConsTecnicos.Open;
  uFuncoes.DimensionarGrid(DBGrid1);
end;

procedure TFrmConsTecnicos.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_ESCAPE then
  Close;
end;

procedure TFrmConsTecnicos.FormResize(Sender: TObject);
begin
  uFuncoes.DimensionarGrid(DBGrid1);
end;

procedure TFrmConsTecnicos.Image1Click(Sender: TObject);
begin
 DM.CDSCadTecnico.Close;
 DM.CDSCadTecnico.Open;
 DM.CDSCadTecnico.Append;
 DM.CDSCadTecnicoID_TECNICO.Value := 0;

 try
    Application.CreateForm(TFrmCadTecnicos, FrmCadTecnicos);
    FrmCadTecnicos.ShowModal;
 finally
   FrmCadTecnicos.Free;
 end;

end;

procedure TFrmConsTecnicos.Image2Click(Sender: TObject);
begin
 DM.CDSCadTecnico.Close;
 DM.CDSCadTecnico.ParamByName('ID_TECNICO').Value := DM.CDSConsTecnicosID_TECNICO.Value;
 DM.CDSCadTecnico.Open;

 if DM.CDSCadTecnico.RecordCount > 0 then
 begin
   DM.CDSCadTecnico.Edit;

    try
      Application.CreateForm(TFrmCadTecnicos, FrmCadTecnicos);
      FrmCadTecnicos.ShowModal;
   finally
     FrmCadTecnicos.Free;
   end;
 end;

end;

end.
