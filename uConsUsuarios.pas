unit uConsUsuarios;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.Imaging.pngimage,
  Vcl.ExtCtrls, Vcl.Grids, Vcl.DBGrids;

type
  TFrmConsUsuarios = class(TForm)
    DBGrid1: TDBGrid;
    Panel1: TPanel;
    Image1: TImage;
    Image2: TImage;
    DSConsUsuarios: TDataSource;
    procedure Image1Click(Sender: TObject);
    procedure Image2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmConsUsuarios: TFrmConsUsuarios;

implementation

{$R *.dfm}

uses uDM, uFuncoes, uCadUsuarios;

procedure TFrmConsUsuarios.FormCreate(Sender: TObject);
begin
  DM.CDSConsUsuarios.Close;
  DM.CDSConsUsuarios.Open;

  uFuncoes.DimensionarGrid(DBGrid1);
end;

procedure TFrmConsUsuarios.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_ESCAPE then
  Close;
end;

procedure TFrmConsUsuarios.FormResize(Sender: TObject);
begin
  uFuncoes.DimensionarGrid(DBGrid1);
end;

procedure TFrmConsUsuarios.Image1Click(Sender: TObject);
begin
 DM.CDSCadUsuario.Close;
 DM.CDSCadUsuario.Open;
 DM.CDSCadUsuario.Append;
 DM.CDSCadUsuarioID_USUARIO.Value := 0;

 try
    Application.CreateForm(TFrmCadUsuarios, FrmCadUsuarios);
    FrmCadUsuarios.ShowModal;
 finally
   FrmCadUsuarios.Free;
 end;

end;

procedure TFrmConsUsuarios.Image2Click(Sender: TObject);
begin
 DM.CDSCadUsuario.Close;
 DM.CDSCadUsuario.ParamByName('ID_USUARIO').Value := DM.CDSConsUsuariosID_USUARIO.Value;
 DM.CDSCadUsuario.Open;

 if DM.CDSCadUsuario.RecordCount > 0 then
 begin
   DM.CDSCadUsuario.Edit;

    try
      Application.CreateForm(TFrmCadUsuarios, FrmCadUsuarios);
      FrmCadUsuarios.ShowModal;
   finally
     FrmCadUsuarios.Free;
   end;
 end;

end;

end.
