unit uOrdemServico;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Imaging.pngimage,
  Vcl.ExtCtrls, Data.DB, Vcl.Grids, Vcl.DBGrids, Vcl.ComCtrls, Vcl.WinXCtrls,
  frxClass, frxDBSet;

type
  TfrmOrdemServico = class(TForm)
    Panel1: TPanel;
    pSummary: TPanel;
    pHeader: TPanel;
    pTotalOs: TPanel;
    Panel4: TPanel;
    Image1: TImage;
    Label1: TLabel;
    LbTotalOS: TLabel;
    pAbertos: TPanel;
    LbAbertosOS: TLabel;
    Panel5: TPanel;
    Image2: TImage;
    Label4: TLabel;
    pExecucacao: TPanel;
    LbExecucaoOS: TLabel;
    Panel6: TPanel;
    Image3: TImage;
    Label6: TLabel;
    pConcluido: TPanel;
    LbConcluidoOS: TLabel;
    Panel7: TPanel;
    Image4: TImage;
    Label8: TLabel;
    DT1: TDateTimePicker;
    DT2: TDateTimePicker;
    Panel2: TPanel;
    Panel3: TPanel;
    DBGrid1: TDBGrid;
    DSConsOS: TDataSource;
    Panel8: TPanel;
    Image5: TImage;
    Image6: TImage;
    Image7: TImage;
    GroupBox2: TGroupBox;
    Label2: TLabel;
    CBFiltro: TComboBox;
    SBPesquisar: TSearchBox;
    CBStatus: TComboBox;
    frxDBConsOS: TfrxDBDataset;
    frxRpConsOS: TfrxReport;
    frxRpConsOSID: TfrxReport;
    frxDBConsOSID: TfrxDBDataset;
    DSConsOSID: TDataSource;
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure Image5Click(Sender: TObject);
    procedure DT1CloseUp(Sender: TObject);
    procedure DT2CloseUp(Sender: TObject);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure Image6Click(Sender: TObject);
    procedure SBPesquisarInvokeSearch(Sender: TObject);
    procedure CBFiltroClick(Sender: TObject);
    procedure CBStatusCloseUp(Sender: TObject);
    procedure Image7Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure DBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public

  Procedure PesquisaOS;
    { Public declarations }
  end;

var
  frmOrdemServico: TfrmOrdemServico;

implementation

{$R *.dfm}

uses uDM, uFuncoes, uCadOS;

procedure TfrmOrdemServico.CBFiltroClick(Sender: TObject);
begin
  if CBFiltro.ItemIndex = 5  then
  begin
    SBPesquisar.Visible := False;
    CBStatus.Visible := True;
  end
  else
  begin
    SBPesquisar.Visible := True;
    CBStatus.Visible := False;
  end;
end;

procedure TfrmOrdemServico.CBStatusCloseUp(Sender: TObject);
begin
 PesquisaOS;
end;

procedure TfrmOrdemServico.DBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
const
clPaleAberto = TColor($0092D43C);
clPaleExecucao = TColor($007777F3);
clPaleConcluido = TColor($00CFC063);
begin

   //ATIVOS
  if Column.Field.Dataset.FieldbyName('STATUS').AsString = 'A'
   then
   begin
       If (gdFocused in State)
	     then
     else DBGrid1.Canvas.brush.color := clPaleAberto;
              DBGrid1.DefaultDrawColumnCell(rect,DataCol,Column,State);
   end;


  //BLOQUEADOS
  if Column.Field.Dataset.FieldbyName('STATUS').AsString = 'E'
   then
   begin
       If (gdFocused in State)
	     then
     else DBGrid1.Canvas.brush.color := clPaleExecucao;
              DBGrid1.DefaultDrawColumnCell(rect,DataCol,Column,State);
   end;

  //INATIVOS
  if Column.Field.Dataset.FieldbyName('STATUS').AsString = 'C'
   then
   begin
       If (gdFocused in State)
	     then
     else DBGrid1.Canvas.brush.color := clPaleConcluido;
              DBGrid1.DefaultDrawColumnCell(rect,DataCol,Column,State);
   end;
end;

procedure TfrmOrdemServico.DBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if Key = VK_DELETE then
 begin
   if MessageBox(0, 'Deseja Excluir a OS?', '', MB_ICONQUESTION or MB_YESNO) = mrYes  then
   begin
    DM.CDSCadOS.Close;
    DM.CDSCadOS.ParamByName('ID_ORDEM').Value := DM.CDSConsOSID_ORDEM.Value;
    DM.CDSCadOS.Open;
    DM.CDSCadOS.Edit;

    DM.CDSCadOS.Delete;
    DM.CDSCadOS.ApplyUpdates(0);

    DM.CDSConsOS.Close;
    DM.CDSConsOS.Open;
   end;
  end;
end;

procedure TfrmOrdemServico.DT1CloseUp(Sender: TObject);
begin
  PesquisaOS;
end;

procedure TfrmOrdemServico.DT2CloseUp(Sender: TObject);
begin
  PesquisaOS;
end;

procedure TfrmOrdemServico.FormCreate(Sender: TObject);
begin
 uFuncoes.DimensionarGrid(DBGrid1);

 DT1.Date := Now - 15;
 DT2.Date := Now;

 PesquisaOS;
end;

procedure TfrmOrdemServico.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_ESCAPE then
  Close;
end;

procedure TfrmOrdemServico.FormResize(Sender: TObject);
begin
 uFuncoes.DimensionarGrid(DBGrid1);
end;

procedure TfrmOrdemServico.Image5Click(Sender: TObject);
begin
  DM.CDSCadOS.Close;
  DM.CDSCadOS.ParamByName('ID_ORDEM').Value := 0;
  DM.CDSCadOS.Open;
  DM.CDSCadOS.Append;

  DM.CDSConsClienteOS.Close;


  try
    Application.CreateForm(TFrmCadOS, FrmCadOS);
    FrmCadOS.Editando := False;
    FrmCadOS.ShowModal;
  finally
    FrmCadOS.Free;
  end;
end;

procedure TfrmOrdemServico.Image6Click(Sender: TObject);
begin
  DM.CDSCadOS.Close;
  DM.CDSCadOS.ParamByName('ID_ORDEM').Value := DM.CDSConsOSID_ORDEM.Value;
  DM.CDSCadOS.Open;
  DM.CDSCadOS.Edit;

  DM.CDSConsClienteOS.Close;
  DM.CDSConsClienteOS.Open;

  DM.CDSConsClienteOS.Filtered := False;
  DM.CDSConsClienteOS.Filter   := 'ID_CLIENTE= ' + DM.CDSConsOSID_CLIENTE.AsString;
  DM.CDSConsClienteOS.Filtered := True;



  try
    Application.CreateForm(TFrmCadOS, FrmCadOS);

    FrmCadOS.Editando := True;

    FrmCadOS.MemoProblema.Text := DM.CDSConsOSPROBLEMA.AsString;
    FrmCadOS.MemoObservacao.Text := DM.CDSConsOSOBSERVACAO.AsString;
    FrmCadOS.EdtSolucaoAdotada.Text := DM.CDSConsOSSOLUCAO_ADOTADA.AsString;

   //--------- STATUS
   if DM.CDSConsOSSTATUS.Value = 'A' then
      FrmCadOS.CBStatus.ItemIndex := 0;

   if DM.CDSConsOSSTATUS.Value = 'E' then
    FrmCadOS.CBStatus.ItemIndex := 1;

   if DM.CDSConsOSSTATUS.Value = 'C' then
    FrmCadOS.CBStatus.ItemIndex := 2;

    FrmCadOS.CBTipo.Text := DM.CDSConsOSTIPO.Value;

    FrmCadOS.BtnSelecionaCliente.Visible := False;
    FrmCadOS.ShowModal;
  finally
    FrmCadOS.Free;
  end;
end;

procedure TfrmOrdemServico.Image7Click(Sender: TObject);
begin
   if MessageBox(0, 'Deseja Imprimir TODAS OS?', '', MB_ICONQUESTION or MB_YESNO) = mrYes  then
    begin
       frxRpConsOS.PrepareReport(True);
       frxRpConsOS.ShowReport;
    end
    else
    begin
      DM.CDSConsOSID.Close;
      DM.CDSConsOSID.ParamByName('ID_ORDEM').Value := DM.CDSConsOSID_ORDEM.Value;
      DM.CDSConsOSID.Open;

      if DM.CDSConsOSID.RecordCount  > 0 then
      begin
       frxRpConsOSID.PrepareReport(True);
       frxRpConsOSID.ShowReport;
      end;

    end;
end;


procedure TfrmOrdemServico.PesquisaOS;
begin
  DM.CDSSummaryOS.Close;
  DM.CDSSummaryOS.ParamByName('DATA_INICIO').Value := DT1.Date;
  DM.CDSSummaryOS.ParamByName('DATA_FIM').Value := DT2.Date;
  DM.CDSSummaryOS.Open;


  frmOrdemServico.LbTotalOS.Caption := DM.CDSSummaryOSTOTAL.AsString;
  frmOrdemServico.LbAbertosOS.Caption := DM.CDSSummaryOSABERTOS.AsString;
  frmOrdemServico.LbExecucaoOS.Caption := DM.CDSSummaryOSEM_EXECUCAO.AsString;
  frmOrdemServico.LbConcluidoOS.Caption := DM.CDSSummaryOSCONCLUIDOS.AsString;


  DM.CDSConsOS.Close;
  DM.CDSConsOS.ParamByName('DATA_INICIO').Value := DT1.Date;
  DM.CDSConsOS.ParamByName('DATA_FIM').Value := DT2.Date;
  DM.CDSConsOS.Open;


  case CBFiltro.ItemIndex of

    {
    Protocolo
    Cliente
    Bairro
    Tipo
    Tecnico
    Status
    }

    0:
    begin
      DM.CDSConsOS.Close;
      DM.CDSConsOS.ParamByName('DATA_INICIO').Value := DT1.Date;
      DM.CDSConsOS.ParamByName('DATA_FIM').Value := DT2.Date;
      DM.CDSConsOS.Open;

      DM.CDSConsOS.Filtered := False;
      DM.CDSConsOS.Filter   := 'ID_ORDEM=' + SBPesquisar.Text;
      DM.CDSConsOS.Filtered := True;

      frmOrdemServico.LbTotalOS.Caption := DM.CDSSummaryOSTOTAL.AsString;
      frmOrdemServico.LbAbertosOS.Caption := DM.CDSSummaryOSABERTOS.AsString;
      frmOrdemServico.LbExecucaoOS.Caption := DM.CDSSummaryOSEM_EXECUCAO.AsString;
      frmOrdemServico.LbConcluidoOS.Caption := DM.CDSSummaryOSCONCLUIDOS.AsString;
    end;

    1:
    begin
      DM.CDSConsOS.Close;
      DM.CDSConsOS.ParamByName('DATA_INICIO').Value := DT1.Date;
      DM.CDSConsOS.ParamByName('DATA_FIM').Value := DT2.Date;
      DM.CDSConsOS.Open;

      DM.CDSConsOS.Filtered := False;
      DM.CDSConsOS.Filter   := 'NOME LIKE ' +  QuotedStr(SBPesquisar.Text + '%');
      DM.CDSConsOS.Filtered := True;

      frmOrdemServico.LbTotalOS.Caption := DM.CDSSummaryOSTOTAL.AsString;
      frmOrdemServico.LbAbertosOS.Caption := DM.CDSSummaryOSABERTOS.AsString;
      frmOrdemServico.LbExecucaoOS.Caption := DM.CDSSummaryOSEM_EXECUCAO.AsString;
      frmOrdemServico.LbConcluidoOS.Caption := DM.CDSSummaryOSCONCLUIDOS.AsString;
    end;


    2:
    begin
      DM.CDSConsOS.Close;
      DM.CDSConsOS.ParamByName('DATA_INICIO').Value := DT1.Date;
      DM.CDSConsOS.ParamByName('DATA_FIM').Value := DT2.Date;
      DM.CDSConsOS.Open;

      DM.CDSConsOS.Filtered := False;
      DM.CDSConsOS.Filter   := 'BAIRRO LIKE ' +  QuotedStr(SBPesquisar.Text + '%');
      DM.CDSConsOS.Filtered := True;

      frmOrdemServico.LbTotalOS.Caption := DM.CDSSummaryOSTOTAL.AsString;
      frmOrdemServico.LbAbertosOS.Caption := DM.CDSSummaryOSABERTOS.AsString;
      frmOrdemServico.LbExecucaoOS.Caption := DM.CDSSummaryOSEM_EXECUCAO.AsString;
      frmOrdemServico.LbConcluidoOS.Caption := DM.CDSSummaryOSCONCLUIDOS.AsString;
    end;


    3:
    begin
      DM.CDSConsOS.Close;
      DM.CDSConsOS.ParamByName('DATA_INICIO').Value := DT1.Date;
      DM.CDSConsOS.ParamByName('DATA_FIM').Value := DT2.Date;
      DM.CDSConsOS.Open;

      DM.CDSConsOS.Filtered := False;
      DM.CDSConsOS.Filter   := 'TIPO LIKE ' +  QuotedStr(SBPesquisar.Text + '%');
      DM.CDSConsOS.Filtered := True;

      frmOrdemServico.LbTotalOS.Caption := DM.CDSSummaryOSTOTAL.AsString;
      frmOrdemServico.LbAbertosOS.Caption := DM.CDSSummaryOSABERTOS.AsString;
      frmOrdemServico.LbExecucaoOS.Caption := DM.CDSSummaryOSEM_EXECUCAO.AsString;
      frmOrdemServico.LbConcluidoOS.Caption := DM.CDSSummaryOSCONCLUIDOS.AsString;
    end;


    4:
    begin
      DM.CDSConsOS.Close;
      DM.CDSConsOS.ParamByName('DATA_INICIO').Value := DT1.Date;
      DM.CDSConsOS.ParamByName('DATA_FIM').Value := DT2.Date;
      DM.CDSConsOS.Open;

      DM.CDSConsOS.Filtered := False;
      DM.CDSConsOS.Filter   := 'TECNICO LIKE ' +  QuotedStr(SBPesquisar.Text + '%');
      DM.CDSConsOS.Filtered := True;

      frmOrdemServico.LbTotalOS.Caption := DM.CDSSummaryOSTOTAL.AsString;
      frmOrdemServico.LbAbertosOS.Caption := DM.CDSSummaryOSABERTOS.AsString;
      frmOrdemServico.LbExecucaoOS.Caption := DM.CDSSummaryOSEM_EXECUCAO.AsString;
      frmOrdemServico.LbConcluidoOS.Caption := DM.CDSSummaryOSCONCLUIDOS.AsString;
    end;


    5:
    begin
      DM.CDSConsOS.Close;
      DM.CDSConsOS.ParamByName('DATA_INICIO').Value := DT1.Date;
      DM.CDSConsOS.ParamByName('DATA_FIM').Value := DT2.Date;
      DM.CDSConsOS.Open;

      DM.CDSConsOS.Filtered := False;

      case CBStatus.ItemIndex of

        0:
        begin
          DM.CDSConsOS.Filter   := 'STATUS =' + QuotedStr('A');
        end;


        1:
        begin
          DM.CDSConsOS.Filter   := 'STATUS =' + QuotedStr('E');
        end;

        2:
        begin
          DM.CDSConsOS.Filter   := 'STATUS =' + QuotedStr('C');
        end;

      end;

      DM.CDSConsOS.Filtered := True;

      frmOrdemServico.LbTotalOS.Caption := DM.CDSSummaryOSTOTAL.AsString;
      frmOrdemServico.LbAbertosOS.Caption := DM.CDSSummaryOSABERTOS.AsString;
      frmOrdemServico.LbExecucaoOS.Caption := DM.CDSSummaryOSEM_EXECUCAO.AsString;
      frmOrdemServico.LbConcluidoOS.Caption := DM.CDSSummaryOSCONCLUIDOS.AsString;
    end;


    else
    begin
      DM.CDSSummaryOS.Close;
      DM.CDSSummaryOS.ParamByName('DATA_INICIO').Value := DT1.Date;
      DM.CDSSummaryOS.ParamByName('DATA_FIM').Value := DT2.Date;
      DM.CDSSummaryOS.Open;

      DM.CDSConsOS.Close;
      DM.CDSConsOS.ParamByName('DATA_INICIO').Value := DT1.Date;
      DM.CDSConsOS.ParamByName('DATA_FIM').Value := DT2.Date;
      DM.CDSConsOS.Open;

      frmOrdemServico.LbTotalOS.Caption := DM.CDSSummaryOSTOTAL.AsString;
      frmOrdemServico.LbAbertosOS.Caption := DM.CDSSummaryOSABERTOS.AsString;
      frmOrdemServico.LbExecucaoOS.Caption := DM.CDSSummaryOSEM_EXECUCAO.AsString;
      frmOrdemServico.LbConcluidoOS.Caption := DM.CDSSummaryOSCONCLUIDOS.AsString;

    end;

  end;


end;

procedure TfrmOrdemServico.SBPesquisarInvokeSearch(Sender: TObject);
begin
PesquisaOS;
end;

end.
