unit uSelecionaClienteOS;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.ExtCtrls, Vcl.Grids,
  Vcl.DBGrids, Vcl.StdCtrls, Vcl.WinXCtrls;

type
  TFrmSelecionaClienteOS = class(TForm)
    DBGrid1: TDBGrid;
    Panel1: TPanel;
    GroupBox2: TGroupBox;
    Label2: TLabel;
    CBFiltro: TComboBox;
    SBPesquisar: TSearchBox;
    DSConsCliente: TDataSource;
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SBPesquisarInvokeSearch(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure DBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
  public
   var IdCliente : Integer;
  end;

var
  FrmSelecionaClienteOS: TFrmSelecionaClienteOS;

implementation

{$R *.dfm}

uses uDM, uFuncoes;

procedure TFrmSelecionaClienteOS.DBGrid1DblClick(Sender: TObject);
begin
 IdCliente := DM.CDSConsClienteOSID_CLIENTE.Value;
 Close;
end;

procedure TFrmSelecionaClienteOS.DBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_RETURN then
  begin
     IdCliente := DM.CDSConsClienteOSID_CLIENTE.Value;
      Close;
  end;

  if Key = VK_ESCAPE then
  begin
    IdCliente := 0;
    Close;
  end;
end;

procedure TFrmSelecionaClienteOS.FormCreate(Sender: TObject);
begin
DM.CDSConsClienteOS.Close;
DM.CDSConsClienteOS.Open;

uFuncoes.DimensionarGrid(DBGrid1);
end;

procedure TFrmSelecionaClienteOS.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = VK_ESCAPE then
  begin
    IdCliente := 0;
    Close;
  end;
end;

procedure TFrmSelecionaClienteOS.FormResize(Sender: TObject);
begin
uFuncoes.DimensionarGrid(DBGrid1);
end;

procedure TFrmSelecionaClienteOS.SBPesquisarInvokeSearch(Sender: TObject);
begin

 {
  Codigo
  Nome
  Endere�o
  Bairro
  Telefone
  Celular
  Status
  Vencimento
  CPF
  RG
 }


  case CBFiltro.ItemIndex of

    0:
    begin
     DM.CDSConsClienteOS.Close;
     DM.CDSConsClienteOS.Open;

     DM.CDSConsClienteOS.Filtered := False;
     DM.CDSConsClienteOS.Filter   := 'ID_CLIENTE='  + QuotedStr(SBPesquisar.Text);
     DM.CDSConsClienteOS.Filtered := True;

     if DM.CDSConsClienteOS.RecordCount = 0 then
        ShowMessage('N�o foi possivel encontrar nada com essa pesquisa');
    end;


   1:
    begin
     DM.CDSConsClienteOS.Close;
     DM.CDSConsClienteOS.Open;

     DM.CDSConsClienteOS.Filtered := False;
     DM.CDSConsClienteOS.Filter   := 'NOME LIKE '  + QuotedStr(SBPesquisar.Text + '%');
     DM.CDSConsClienteOS.Filtered := True;

     if DM.CDSConsClienteOS.RecordCount = 0 then
        ShowMessage('N�o foi possivel encontrar nada com essa pesquisa');
    end;

    2:
    begin
     DM.CDSConsClienteOS.Close;
     DM.CDSConsClienteOS.Open;

     DM.CDSConsClienteOS.Filtered := False;
     DM.CDSConsClienteOS.Filter   := 'ENDERECO LIKE '  + QuotedStr(SBPesquisar.Text + '%');
     DM.CDSConsClienteOS.Filtered := True;

     if DM.CDSConsClienteOS.RecordCount = 0 then
        ShowMessage('N�o foi possivel encontrar nada com essa pesquisa');
    end;

    3:
    begin
     DM.CDSConsClienteOS.Close;
     DM.CDSConsClienteOS.Open;

     DM.CDSConsClienteOS.Filtered := False;
     DM.CDSConsClienteOS.Filter   := 'BAIRRO LIKE '  + QuotedStr(SBPesquisar.Text + '%');
     DM.CDSConsClienteOS.Filtered := True;

     if DM.CDSConsClienteOS.RecordCount = 0 then
        ShowMessage('N�o foi possivel encontrar nada com essa pesquisa');
    end;


    4:
    begin
     DM.CDSConsClienteOS.Close;
     DM.CDSConsClienteOS.Open;

     DM.CDSConsClienteOS.Filtered := False;
     DM.CDSConsClienteOS.Filter   := 'TELEFONE LIKE '  + QuotedStr(SBPesquisar.Text + '%');
     DM.CDSConsClienteOS.Filtered := True;

     if DM.CDSConsClienteOS.RecordCount = 0 then
        ShowMessage('N�o foi possivel encontrar nada com essa pesquisa');
    end;


    5:
    begin
     DM.CDSConsClienteOS.Close;
     DM.CDSConsClienteOS.Open;

     DM.CDSConsClienteOS.Filtered := False;
     DM.CDSConsClienteOS.Filter   := 'CELULAR LIKE '  + QuotedStr(SBPesquisar.Text + '%');
     DM.CDSConsClienteOS.Filtered := True;

     if DM.CDSConsClienteOS.RecordCount = 0 then
        ShowMessage('N�o foi possivel encontrar nada com essa pesquisa');
    end;

    6:
    begin
     DM.CDSConsClienteOS.Close;
     DM.CDSConsClienteOS.Open;

     DM.CDSConsClienteOS.Filtered := False;
     DM.CDSConsClienteOS.Filter   := 'STATUS='  + QuotedStr(SBPesquisar.Text);
     DM.CDSConsClienteOS.Filtered := True;

     if DM.CDSConsClienteOS.RecordCount = 0 then
        ShowMessage('N�o foi possivel encontrar nada com essa pesquisa');
    end;

    7:
    begin
     DM.CDSConsClienteOS.Close;
     DM.CDSConsClienteOS.Open;

     DM.CDSConsClienteOS.Filtered := False;
     DM.CDSConsClienteOS.Filter   := 'VENCIMENTO='  + QuotedStr(SBPesquisar.Text);
     DM.CDSConsClienteOS.Filtered := True;

     if DM.CDSConsClienteOS.RecordCount = 0 then
        ShowMessage('N�o foi possivel encontrar nada com essa pesquisa');
    end;

    8:
    begin
     DM.CDSConsClienteOS.Close;
     DM.CDSConsClienteOS.Open;

     DM.CDSConsClienteOS.Filtered := False;
     DM.CDSConsClienteOS.Filter   := 'CPF='  + QuotedStr(SBPesquisar.Text);
     DM.CDSConsClienteOS.Filtered := True;

     if DM.CDSConsClienteOS.RecordCount = 0 then
        ShowMessage('N�o foi possivel encontrar nada com essa pesquisa');
    end;


    9:
    begin
     DM.CDSConsClienteOS.Close;
     DM.CDSConsClienteOS.Open;

     DM.CDSConsClienteOS.Filtered := False;
     DM.CDSConsClienteOS.Filter   := 'RG='  + QuotedStr(SBPesquisar.Text);
     DM.CDSConsClienteOS.Filtered := True;

     if DM.CDSConsClienteOS.RecordCount = 0 then
        ShowMessage('N�o foi possivel encontrar nada com essa pesquisa');
    end;

    else
    begin
       ShowMessage('Selecione o Filtro antes da Pesquisa.');
    end;
  end;
end;

end.
