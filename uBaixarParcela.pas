unit uBaixarParcela;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls,
  Vcl.Imaging.pngimage;

type
  TfrmBaixarParcela = class(TForm)
    lbVParcela: TLabel;
    Label3: TLabel;
    lbNParcela: TLabel;
    lbNomeCliente: TLabel;
    RGDesconto: TRadioGroup;
    Panel1: TPanel;
    Image1: TImage;
    Image2: TImage;
    Image3: TImage;
    RGPix: TRadioGroup;
    procedure Image1Click(Sender: TObject);
    procedure Image2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmBaixarParcela: TfrmBaixarParcela;

implementation

{$R *.dfm}

uses uDM;

procedure TfrmBaixarParcela.Image1Click(Sender: TObject);
var Pix : String;
begin
  Pix := '';
  if RGDesconto.ItemIndex = -1 then
  begin
    ShowMessage('Selecione se foi com desconto ou sem desconto');
    Exit;
  end;

  if RGPix.ItemIndex = 0 then
     Pix := ' | [PIX]';


  DM.CDSConfig.Close;
  DM.CDSConfig.Open;

  DM.CDSEditParcelaDATA_PAGAMENTO.Value := Now;
  DM.CDSEditParcelaSTATUS.Value := 'P';
  DM.CDSEditParcelaOBSERVACAO.Value := 'Parcela Baixada por: ' + DM.NOME_USUARIO + ' | ' + DateTimeToStr(Now) + Pix;

  if RGDesconto.ItemIndex = 0 then
     DM.CDSEditParcelaVALOR.Value := DM.CDSEditParcelaVALOR.Value - DM.CDSConfigVALOR_DESCONTO.Value;

  DM.CDSEditParcela.Post;
  DM.CDSEditParcela.ApplyUpdates(0);

  DM.CDSConsParcelas.Close;
  DM.CDSConsParcelas.ParamByName('ID_CLIENTE').Value := DM.CDSConsClienteParcelaID_CLIENTE.Value;
  DM.CDSConsParcelas.Open;

  ShowMessage('Parcela Baixada com Sucesso!');
  Close;
end;

procedure TfrmBaixarParcela.Image2Click(Sender: TObject);
begin
  DM.CDSCadParcela.Cancel;
  Close;
end;

end.
