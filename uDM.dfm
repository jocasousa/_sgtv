object DM: TDM
  OldCreateOrder = False
  Height = 829
  Width = 1119
  object FDConsCliente: TFDQuery
    Connection = FDConn
    SQL.Strings = (
      'SELECT * FROM CLIENTES')
    Left = 96
    Top = 152
  end
  object FDConn: TFDConnection
    Params.Strings = (
      'User_Name=sysdba'
      'Password=masterkey'
      'DriverID=FB')
    LoginPrompt = False
    BeforeConnect = FDConnBeforeConnect
    Left = 16
    Top = 8
  end
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'Forms'
    Left = 1008
    Top = 8
  end
  object FDPhysFBDriverLink1: TFDPhysFBDriverLink
    VendorLib = 'fbclient.dll'
    Left = 1048
    Top = 8
  end
  object DSPConsCliente: TDataSetProvider
    DataSet = FDConsCliente
    Left = 200
    Top = 152
  end
  object CDSConsCliente: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DSPConsCliente'
    Left = 304
    Top = 152
    object CDSConsClienteID_CLIENTE: TIntegerField
      FieldName = 'ID_CLIENTE'
      Required = True
    end
    object CDSConsClienteNOME: TStringField
      FieldName = 'NOME'
      Size = 100
    end
    object CDSConsClienteENDERECO: TStringField
      FieldName = 'ENDERECO'
      Size = 50
    end
    object CDSConsClienteNUMERO: TStringField
      FieldName = 'NUMERO'
      Size = 10
    end
    object CDSConsClienteBAIRRO: TStringField
      FieldName = 'BAIRRO'
    end
    object CDSConsClienteCIDADE: TStringField
      FieldName = 'CIDADE'
      Size = 15
    end
    object CDSConsClienteUF: TStringField
      FieldName = 'UF'
      Size = 2
    end
    object CDSConsClienteCEP: TStringField
      FieldName = 'CEP'
      Size = 30
    end
    object CDSConsClienteREFERENCIA: TStringField
      FieldName = 'REFERENCIA'
      Size = 500
    end
    object CDSConsClienteCPF_CNPJ: TStringField
      FieldName = 'CPF_CNPJ'
    end
    object CDSConsClienteRG_IE: TStringField
      FieldName = 'RG_IE'
    end
    object CDSConsClienteEMAIL: TStringField
      FieldName = 'EMAIL'
      Size = 50
    end
    object CDSConsClienteVENCIMENTO: TStringField
      FieldName = 'VENCIMENTO'
      Size = 10
    end
    object CDSConsClienteOBSERVACAO: TStringField
      FieldName = 'OBSERVACAO'
      Size = 1000
    end
    object CDSConsClienteTEL1: TStringField
      FieldName = 'TEL1'
      Size = 15
    end
    object CDSConsClienteTEL2: TStringField
      FieldName = 'TEL2'
      Size = 15
    end
    object CDSConsClienteCELULAR: TStringField
      FieldName = 'CELULAR'
      Size = 15
    end
    object CDSConsClienteVALORMENSALIDADE: TFloatField
      FieldName = 'VALORMENSALIDADE'
    end
    object CDSConsClienteDATA_VENCIMENTO: TDateField
      FieldName = 'DATA_VENCIMENTO'
    end
    object CDSConsClienteSTATUS: TStringField
      FieldName = 'STATUS'
      Size = 12
    end
    object CDSConsClienteNASCIMENTO: TDateField
      FieldName = 'NASCIMENTO'
    end
    object CDSConsClienteDATA_INSTALACAO: TDateField
      FieldName = 'DATA_INSTALACAO'
    end
    object CDSConsClienteINADIMPLENCIA: TStringField
      FieldName = 'INADIMPLENCIA'
      Size = 100
    end
    object CDSConsClientePARCELAS_GERADAS: TStringField
      FieldName = 'PARCELAS_GERADAS'
      Size = 1
    end
  end
  object FDCadCliente: TFDQuery
    Connection = FDConn
    SQL.Strings = (
      'SELECT * FROM CLIENTES WHERE ID_CLIENTE = :ID_CLIENTE')
    Left = 96
    Top = 232
    ParamData = <
      item
        Position = 1
        Name = 'ID_CLIENTE'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object DSPCadCliente: TDataSetProvider
    DataSet = FDCadCliente
    Left = 200
    Top = 232
  end
  object CDSCadCliente: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'ID_CLIENTE'
        ParamType = ptInput
      end>
    ProviderName = 'DSPCadCliente'
    Left = 304
    Top = 232
    object CDSCadClienteID_CLIENTE: TIntegerField
      FieldName = 'ID_CLIENTE'
      Required = True
    end
    object CDSCadClienteNOME: TStringField
      FieldName = 'NOME'
      Size = 100
    end
    object CDSCadClienteENDERECO: TStringField
      FieldName = 'ENDERECO'
      Size = 50
    end
    object CDSCadClienteNUMERO: TStringField
      FieldName = 'NUMERO'
      Size = 10
    end
    object CDSCadClienteBAIRRO: TStringField
      FieldName = 'BAIRRO'
    end
    object CDSCadClienteCIDADE: TStringField
      FieldName = 'CIDADE'
      Size = 15
    end
    object CDSCadClienteUF: TStringField
      FieldName = 'UF'
      Size = 2
    end
    object CDSCadClienteCEP: TStringField
      FieldName = 'CEP'
      Size = 30
    end
    object CDSCadClienteREFERENCIA: TStringField
      FieldName = 'REFERENCIA'
      Size = 500
    end
    object CDSCadClienteCPF_CNPJ: TStringField
      FieldName = 'CPF_CNPJ'
    end
    object CDSCadClienteRG_IE: TStringField
      FieldName = 'RG_IE'
    end
    object CDSCadClienteEMAIL: TStringField
      FieldName = 'EMAIL'
      Size = 50
    end
    object CDSCadClienteVENCIMENTO: TStringField
      FieldName = 'VENCIMENTO'
      Size = 10
    end
    object CDSCadClienteOBSERVACAO: TStringField
      FieldName = 'OBSERVACAO'
      Size = 1000
    end
    object CDSCadClienteTEL1: TStringField
      FieldName = 'TEL1'
      Size = 15
    end
    object CDSCadClienteTEL2: TStringField
      FieldName = 'TEL2'
      Size = 15
    end
    object CDSCadClienteCELULAR: TStringField
      FieldName = 'CELULAR'
      Size = 15
    end
    object CDSCadClienteVALORMENSALIDADE: TFloatField
      FieldName = 'VALORMENSALIDADE'
    end
    object CDSCadClienteDATA_VENCIMENTO: TDateField
      FieldName = 'DATA_VENCIMENTO'
    end
    object CDSCadClienteSTATUS: TStringField
      FieldName = 'STATUS'
      Size = 12
    end
    object CDSCadClienteNASCIMENTO: TDateField
      FieldName = 'NASCIMENTO'
    end
    object CDSCadClienteDATA_INSTALACAO: TDateField
      FieldName = 'DATA_INSTALACAO'
    end
    object CDSCadClienteINADIMPLENCIA: TStringField
      FieldName = 'INADIMPLENCIA'
      Size = 100
    end
    object CDSCadClientePARCELAS_GERADAS: TStringField
      FieldName = 'PARCELAS_GERADAS'
      Size = 1
    end
  end
  object FDControle: TFDQuery
    Connection = FDConn
    SQL.Strings = (
      'SELECT * FROM CONTROLE WHERE CAMPO = :CAMPO')
    Left = 96
    Top = 72
    ParamData = <
      item
        Name = 'CAMPO'
        DataType = ftString
        ParamType = ptInput
        Size = 50
        Value = Null
      end>
  end
  object DSPControle: TDataSetProvider
    DataSet = FDControle
    Left = 200
    Top = 72
  end
  object CDSControle: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftString
        Name = 'CAMPO'
        ParamType = ptInput
        Size = 50
      end>
    ProviderName = 'DSPControle'
    Left = 304
    Top = 72
    object CDSControleCAMPO: TStringField
      FieldName = 'CAMPO'
      Size = 50
    end
    object CDSControleVALOR: TIntegerField
      FieldName = 'VALOR'
    end
  end
  object FDConsParcelas: TFDQuery
    Connection = FDConn
    SQL.Strings = (
      'SELECT * FROM PARCELAS'
      'WHERE ID_CLIENTE = :ID_CLIENTE')
    Left = 96
    Top = 312
    ParamData = <
      item
        Position = 1
        Name = 'ID_CLIENTE'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object DSPConsParcelas: TDataSetProvider
    DataSet = FDConsParcelas
    Left = 200
    Top = 312
  end
  object CDSConsParcelas: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'ID_CLIENTE'
        ParamType = ptInput
      end>
    ProviderName = 'DSPConsParcelas'
    Left = 304
    Top = 312
    object CDSConsParcelasID_PARCELA: TIntegerField
      FieldName = 'ID_PARCELA'
      Required = True
    end
    object CDSConsParcelasID_CLIENTE: TIntegerField
      FieldName = 'ID_CLIENTE'
    end
    object CDSConsParcelasVALOR: TFloatField
      FieldName = 'VALOR'
      currency = True
    end
    object CDSConsParcelasNUM_PARCELA: TIntegerField
      FieldName = 'NUM_PARCELA'
    end
    object CDSConsParcelasDATA_PAGAMENTO: TDateField
      FieldName = 'DATA_PAGAMENTO'
    end
    object CDSConsParcelasDESCONTO: TStringField
      FieldName = 'DESCONTO'
      Size = 1
    end
    object CDSConsParcelasOBSERVACAO: TStringField
      FieldName = 'OBSERVACAO'
      Size = 100
    end
    object CDSConsParcelasSTATUS: TStringField
      FieldName = 'STATUS'
      Size = 10
    end
    object CDSConsParcelasDATA_VENCIMENTO: TDateField
      FieldName = 'DATA_VENCIMENTO'
    end
  end
  object FDConsClienteParcela: TFDQuery
    Connection = FDConn
    SQL.Strings = (
      'SELECT * FROM CLIENTES')
    Left = 96
    Top = 472
  end
  object DSPConsClienteParcela: TDataSetProvider
    DataSet = FDConsClienteParcela
    Left = 200
    Top = 472
  end
  object CDSConsClienteParcela: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DSPConsClienteParcela'
    Left = 304
    Top = 472
    object CDSConsClienteParcelaID_CLIENTE: TIntegerField
      FieldName = 'ID_CLIENTE'
      Required = True
    end
    object CDSConsClienteParcelaNOME: TStringField
      FieldName = 'NOME'
      Size = 100
    end
    object CDSConsClienteParcelaENDERECO: TStringField
      FieldName = 'ENDERECO'
      Size = 50
    end
    object CDSConsClienteParcelaNUMERO: TStringField
      FieldName = 'NUMERO'
      Size = 10
    end
    object CDSConsClienteParcelaBAIRRO: TStringField
      FieldName = 'BAIRRO'
    end
    object CDSConsClienteParcelaCIDADE: TStringField
      FieldName = 'CIDADE'
      Size = 15
    end
    object CDSConsClienteParcelaUF: TStringField
      FieldName = 'UF'
      Size = 2
    end
    object CDSConsClienteParcelaCEP: TStringField
      FieldName = 'CEP'
      Size = 30
    end
    object CDSConsClienteParcelaREFERENCIA: TStringField
      FieldName = 'REFERENCIA'
      Size = 500
    end
    object CDSConsClienteParcelaCPF_CNPJ: TStringField
      FieldName = 'CPF_CNPJ'
    end
    object CDSConsClienteParcelaRG_IE: TStringField
      FieldName = 'RG_IE'
    end
    object CDSConsClienteParcelaEMAIL: TStringField
      FieldName = 'EMAIL'
      Size = 50
    end
    object CDSConsClienteParcelaVENCIMENTO: TStringField
      FieldName = 'VENCIMENTO'
      Size = 10
    end
    object CDSConsClienteParcelaOBSERVACAO: TStringField
      FieldName = 'OBSERVACAO'
      Size = 1000
    end
    object CDSConsClienteParcelaTEL1: TStringField
      FieldName = 'TEL1'
      Size = 15
    end
    object CDSConsClienteParcelaTEL2: TStringField
      FieldName = 'TEL2'
      Size = 15
    end
    object CDSConsClienteParcelaCELULAR: TStringField
      FieldName = 'CELULAR'
      Size = 15
    end
    object CDSConsClienteParcelaVALORMENSALIDADE: TFloatField
      FieldName = 'VALORMENSALIDADE'
    end
    object CDSConsClienteParcelaDATA_VENCIMENTO: TDateField
      FieldName = 'DATA_VENCIMENTO'
    end
    object CDSConsClienteParcelaSTATUS: TStringField
      FieldName = 'STATUS'
      Size = 12
    end
    object CDSConsClienteParcelaNASCIMENTO: TDateField
      FieldName = 'NASCIMENTO'
    end
    object CDSConsClienteParcelaDATA_INSTALACAO: TDateField
      FieldName = 'DATA_INSTALACAO'
    end
    object CDSConsClienteParcelaINADIMPLENCIA: TStringField
      FieldName = 'INADIMPLENCIA'
      Size = 100
    end
    object CDSConsClienteParcelaPARCELAS_GERADAS: TStringField
      FieldName = 'PARCELAS_GERADAS'
      Size = 1
    end
  end
  object FDCadParcela: TFDQuery
    Connection = FDConn
    SQL.Strings = (
      'SELECT * FROM PARCELAS'
      'WHERE ID_CLIENTE = :ID_CLIENTE')
    Left = 96
    Top = 392
    ParamData = <
      item
        Position = 1
        Name = 'ID_CLIENTE'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object DSPCadParcela: TDataSetProvider
    DataSet = FDCadParcela
    Left = 200
    Top = 392
  end
  object CDSCadParcela: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'ID_CLIENTE'
        ParamType = ptInput
      end>
    ProviderName = 'DSPCadParcela'
    Left = 304
    Top = 392
    object CDSCadParcelaID_PARCELA: TIntegerField
      FieldName = 'ID_PARCELA'
      Required = True
    end
    object CDSCadParcelaID_CLIENTE: TIntegerField
      FieldName = 'ID_CLIENTE'
    end
    object CDSCadParcelaVALOR: TFloatField
      FieldName = 'VALOR'
    end
    object CDSCadParcelaNUM_PARCELA: TIntegerField
      FieldName = 'NUM_PARCELA'
    end
    object CDSCadParcelaDATA_PAGAMENTO: TDateField
      FieldName = 'DATA_PAGAMENTO'
    end
    object CDSCadParcelaDESCONTO: TStringField
      FieldName = 'DESCONTO'
      Size = 1
    end
    object CDSCadParcelaOBSERVACAO: TStringField
      FieldName = 'OBSERVACAO'
      Size = 100
    end
    object CDSCadParcelaSTATUS: TStringField
      FieldName = 'STATUS'
      Size = 10
    end
    object CDSCadParcelaDATA_VENCIMENTO: TDateField
      FieldName = 'DATA_VENCIMENTO'
    end
  end
  object FDConfig: TFDQuery
    Connection = FDConn
    SQL.Strings = (
      'SELECT * FROM CONFIG')
    Left = 96
    Top = 552
  end
  object DSPConfig: TDataSetProvider
    DataSet = FDConfig
    Left = 200
    Top = 552
  end
  object CDSConfig: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DSPConfig'
    Left = 304
    Top = 552
    object CDSConfigVALOR_MENSALIDADE: TFloatField
      FieldName = 'VALOR_MENSALIDADE'
    end
    object CDSConfigVALOR_DESCONTO: TFloatField
      FieldName = 'VALOR_DESCONTO'
    end
  end
  object FDEditParcela: TFDQuery
    Connection = FDConn
    SQL.Strings = (
      'SELECT * FROM PARCELAS WHERE ID_PARCELA = :ID_PARCELA')
    Left = 96
    Top = 632
    ParamData = <
      item
        Name = 'ID_PARCELA'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
  end
  object DSPEditParcela: TDataSetProvider
    DataSet = FDEditParcela
    Left = 200
    Top = 632
  end
  object CDSEditParcela: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'ID_PARCELA'
        ParamType = ptInput
      end>
    ProviderName = 'DSPEditParcela'
    Left = 304
    Top = 632
    object CDSEditParcelaID_PARCELA: TIntegerField
      FieldName = 'ID_PARCELA'
      Required = True
    end
    object CDSEditParcelaID_CLIENTE: TIntegerField
      FieldName = 'ID_CLIENTE'
    end
    object CDSEditParcelaVALOR: TFloatField
      FieldName = 'VALOR'
    end
    object CDSEditParcelaNUM_PARCELA: TIntegerField
      FieldName = 'NUM_PARCELA'
    end
    object CDSEditParcelaDATA_VENCIMENTO: TDateField
      FieldName = 'DATA_VENCIMENTO'
    end
    object CDSEditParcelaDATA_PAGAMENTO: TDateField
      FieldName = 'DATA_PAGAMENTO'
    end
    object CDSEditParcelaDESCONTO: TStringField
      FieldName = 'DESCONTO'
      Size = 1
    end
    object CDSEditParcelaOBSERVACAO: TStringField
      FieldName = 'OBSERVACAO'
      Size = 100
    end
    object CDSEditParcelaSTATUS: TStringField
      FieldName = 'STATUS'
      Size = 10
    end
  end
  object FDRecibo: TFDQuery
    Connection = FDConn
    SQL.Strings = (
      'SELECT'
      '    PARCELAS.ID_CLIENTE,'
      '    CLIENTES.NOME,'
      '    CLIENTES.BAIRRO,'
      '    CLIENTES.ENDERECO,'
      '    PARCELAS.DATA_VENCIMENTO,'
      '    PARCELAS.VALOR,'
      '    PARCELAS.STATUS,'
      '    CONFIG.MENSAGEM_CARNE,'
      '    CONFIG.VALOR_MENSALIDADE,'
      '    CONFIG.VALOR_DESCONTO'
      'FROM'
      '    PARCELAS, CONFIG'
      
        'INNER JOIN CLIENTES ON (PARCELAS.ID_CLIENTE = CLIENTES.ID_CLIENT' +
        'E)'
      'WHERE PARCELAS.ID_CLIENTE = :ID_CLIENTE')
    Left = 648
    Top = 80
    ParamData = <
      item
        Position = 1
        Name = 'ID_CLIENTE'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object DSPRecibo: TDataSetProvider
    DataSet = FDRecibo
    Left = 768
    Top = 80
  end
  object CDSRecibo: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'ID_CLIENTE'
        ParamType = ptInput
      end>
    ProviderName = 'DSPRecibo'
    Left = 872
    Top = 80
    object CDSReciboID_CLIENTE: TIntegerField
      FieldName = 'ID_CLIENTE'
      Origin = 'ID_CLIENTE'
    end
    object CDSReciboNOME: TStringField
      FieldName = 'NOME'
      Origin = 'NOME'
      ProviderFlags = []
      ReadOnly = True
      Size = 100
    end
    object CDSReciboBAIRRO: TStringField
      FieldName = 'BAIRRO'
      Origin = 'BAIRRO'
      ProviderFlags = []
      ReadOnly = True
    end
    object CDSReciboENDERECO: TStringField
      FieldName = 'ENDERECO'
      Origin = 'ENDERECO'
      ProviderFlags = []
      ReadOnly = True
      Size = 50
    end
    object CDSReciboDATA_VENCIMENTO: TDateField
      FieldName = 'DATA_VENCIMENTO'
      Origin = 'DATA_VENCIMENTO'
    end
    object CDSReciboVALOR: TFloatField
      FieldName = 'VALOR'
      Origin = 'VALOR'
      currency = True
    end
    object CDSReciboMENSAGEM_CARNE: TStringField
      FieldName = 'MENSAGEM_CARNE'
      Origin = 'MENSAGEM_CARNE'
      ProviderFlags = []
      ReadOnly = True
      Size = 1000
    end
    object CDSReciboVALOR_MENSALIDADE: TFloatField
      FieldName = 'VALOR_MENSALIDADE'
      Origin = 'VALOR_MENSALIDADE'
      ProviderFlags = []
      ReadOnly = True
      currency = True
    end
    object CDSReciboVALOR_DESCONTO: TFloatField
      FieldName = 'VALOR_DESCONTO'
      Origin = 'VALOR_DESCONTO'
      ProviderFlags = []
      ReadOnly = True
      currency = True
    end
    object CDSReciboSTATUS: TStringField
      FieldName = 'STATUS'
      Origin = 'STATUS'
      Size = 10
    end
  end
  object FDConsOS: TFDQuery
    Connection = FDConn
    SQL.Strings = (
      'SELECT'
      '    ORDEM_SERVICO.*,'
      '    CLIENTES.NOME,'
      '    CLIENTES.ENDERECO,'
      '    CLIENTES.BAIRRO,'
      '    CLIENTES.REFERENCIA,'
      '    CLIENTES.TEL1,'
      '    CLIENTES.CELULAR,'
      '    TECNICOS.ID_TECNICO,'
      '    TECNICOS.NOME AS TECNICO,'
      '    TECNICOS.CELULAR'
      ' FROM ORDEM_SERVICO'
      
        'INNER JOIN CLIENTES ON (ORDEM_SERVICO.ID_CLIENTE = CLIENTES.ID_C' +
        'LIENTE)'
      
        'INNER JOIN TECNICOS ON (ORDEM_SERVICO.ID_TECNICO = TECNICOS.ID_T' +
        'ECNICO)'
      'WHERE'
      'ORDEM_SERVICO.DT_ABERTURA BETWEEN :DATA_INICIO AND :DATA_FIM'
      ''
      ''
      '')
    Left = 648
    Top = 160
    ParamData = <
      item
        Name = 'DATA_INICIO'
        DataType = ftDate
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'DATA_FIM'
        DataType = ftDate
        ParamType = ptInput
      end>
  end
  object DSPConsOS: TDataSetProvider
    DataSet = FDConsOS
    Left = 768
    Top = 160
  end
  object CDSConsOS: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftDate
        Name = 'DATA_INICIO'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'DATA_FIM'
        ParamType = ptInput
      end>
    ProviderName = 'DSPConsOS'
    Left = 872
    Top = 160
    object CDSConsOSID_ORDEM: TIntegerField
      FieldName = 'ID_ORDEM'
      Required = True
    end
    object CDSConsOSID_CLIENTE: TIntegerField
      FieldName = 'ID_CLIENTE'
    end
    object CDSConsOSID_TECNICO: TIntegerField
      FieldName = 'ID_TECNICO'
    end
    object CDSConsOSTIPO: TStringField
      FieldName = 'TIPO'
      Size = 50
    end
    object CDSConsOSPROBLEMA: TStringField
      FieldName = 'PROBLEMA'
      Size = 150
    end
    object CDSConsOSDT_ABERTURA: TDateField
      FieldName = 'DT_ABERTURA'
    end
    object CDSConsOSHR_ABERTURA: TTimeField
      FieldName = 'HR_ABERTURA'
    end
    object CDSConsOSDT_FINAL: TDateField
      FieldName = 'DT_FINAL'
    end
    object CDSConsOSHR_FINAL: TTimeField
      FieldName = 'HR_FINAL'
    end
    object CDSConsOSSOLUCAO_ADOTADA: TStringField
      FieldName = 'SOLUCAO_ADOTADA'
      Size = 500
    end
    object CDSConsOSOBSERVACAO: TStringField
      FieldName = 'OBSERVACAO'
      Size = 500
    end
    object CDSConsOSDT_FECHAMENTO: TDateField
      FieldName = 'DT_FECHAMENTO'
    end
    object CDSConsOSHR_FECHAMENTO: TTimeField
      FieldName = 'HR_FECHAMENTO'
    end
    object CDSConsOSSTATUS: TStringField
      FieldName = 'STATUS'
      Size = 1
    end
    object CDSConsOSNOME: TStringField
      FieldName = 'NOME'
      ReadOnly = True
      Size = 100
    end
    object CDSConsOSENDERECO: TStringField
      FieldName = 'ENDERECO'
      ReadOnly = True
      Size = 50
    end
    object CDSConsOSBAIRRO: TStringField
      FieldName = 'BAIRRO'
      ReadOnly = True
    end
    object CDSConsOSREFERENCIA: TStringField
      FieldName = 'REFERENCIA'
      ReadOnly = True
      Size = 500
    end
    object CDSConsOSTEL1: TStringField
      FieldName = 'TEL1'
      ReadOnly = True
      Size = 15
    end
    object CDSConsOSCELULAR: TStringField
      FieldName = 'CELULAR'
      ReadOnly = True
      Size = 15
    end
    object CDSConsOSID_TECNICO_1: TIntegerField
      FieldName = 'ID_TECNICO_1'
      ReadOnly = True
    end
    object CDSConsOSTECNICO: TStringField
      FieldName = 'TECNICO'
      ReadOnly = True
      Size = 100
    end
    object CDSConsOSCELULAR_1: TStringField
      FieldName = 'CELULAR_1'
      ReadOnly = True
      Size = 25
    end
  end
  object FDCadOS: TFDQuery
    Connection = FDConn
    SQL.Strings = (
      'SELECT * FROM ORDEM_SERVICO WHERE ID_ORDEM = :ID_ORDEM')
    Left = 648
    Top = 232
    ParamData = <
      item
        Position = 1
        Name = 'ID_ORDEM'
        DataType = ftString
        ParamType = ptInput
        Size = 15
      end>
  end
  object DSPCadOS: TDataSetProvider
    DataSet = FDCadOS
    Left = 768
    Top = 232
  end
  object CDSCadOS: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftString
        Name = 'ID_ORDEM'
        ParamType = ptInput
        Size = 15
      end>
    ProviderName = 'DSPCadOS'
    Left = 872
    Top = 240
    object CDSCadOSID_ORDEM: TIntegerField
      FieldName = 'ID_ORDEM'
      Required = True
    end
    object CDSCadOSID_CLIENTE: TIntegerField
      FieldName = 'ID_CLIENTE'
    end
    object CDSCadOSID_TECNICO: TIntegerField
      FieldName = 'ID_TECNICO'
    end
    object CDSCadOSTIPO: TStringField
      FieldName = 'TIPO'
      Size = 50
    end
    object CDSCadOSPROBLEMA: TStringField
      FieldName = 'PROBLEMA'
      Size = 150
    end
    object CDSCadOSDT_ABERTURA: TDateField
      FieldName = 'DT_ABERTURA'
    end
    object CDSCadOSHR_ABERTURA: TTimeField
      FieldName = 'HR_ABERTURA'
    end
    object CDSCadOSDT_FINAL: TDateField
      FieldName = 'DT_FINAL'
    end
    object CDSCadOSHR_FINAL: TTimeField
      FieldName = 'HR_FINAL'
    end
    object CDSCadOSSOLUCAO_ADOTADA: TStringField
      FieldName = 'SOLUCAO_ADOTADA'
      Size = 500
    end
    object CDSCadOSOBSERVACAO: TStringField
      FieldName = 'OBSERVACAO'
      Size = 500
    end
    object CDSCadOSDT_FECHAMENTO: TDateField
      FieldName = 'DT_FECHAMENTO'
    end
    object CDSCadOSHR_FECHAMENTO: TTimeField
      FieldName = 'HR_FECHAMENTO'
    end
    object CDSCadOSSTATUS: TStringField
      FieldName = 'STATUS'
      Size = 1
    end
  end
  object FDConsClienteOS: TFDQuery
    Connection = FDConn
    SQL.Strings = (
      'SELECT * FROM CLIENTES')
    Left = 648
    Top = 296
  end
  object DSPConsClienteOS: TDataSetProvider
    DataSet = FDConsClienteOS
    Left = 768
    Top = 296
  end
  object CDSConsClienteOS: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DSPConsClienteOS'
    Left = 872
    Top = 304
    object CDSConsClienteOSID_CLIENTE: TIntegerField
      FieldName = 'ID_CLIENTE'
      Required = True
    end
    object CDSConsClienteOSNOME: TStringField
      FieldName = 'NOME'
      Size = 100
    end
    object CDSConsClienteOSENDERECO: TStringField
      FieldName = 'ENDERECO'
      Size = 50
    end
    object CDSConsClienteOSNUMERO: TStringField
      FieldName = 'NUMERO'
      Size = 10
    end
    object CDSConsClienteOSBAIRRO: TStringField
      FieldName = 'BAIRRO'
    end
    object CDSConsClienteOSCIDADE: TStringField
      FieldName = 'CIDADE'
      Size = 15
    end
    object CDSConsClienteOSUF: TStringField
      FieldName = 'UF'
      Size = 2
    end
    object CDSConsClienteOSCEP: TStringField
      FieldName = 'CEP'
      Size = 30
    end
    object CDSConsClienteOSREFERENCIA: TStringField
      FieldName = 'REFERENCIA'
      Size = 500
    end
    object CDSConsClienteOSCPF_CNPJ: TStringField
      FieldName = 'CPF_CNPJ'
    end
    object CDSConsClienteOSRG_IE: TStringField
      FieldName = 'RG_IE'
    end
    object CDSConsClienteOSEMAIL: TStringField
      FieldName = 'EMAIL'
      Size = 50
    end
    object CDSConsClienteOSVENCIMENTO: TStringField
      FieldName = 'VENCIMENTO'
      Size = 10
    end
    object CDSConsClienteOSOBSERVACAO: TStringField
      FieldName = 'OBSERVACAO'
      Size = 1000
    end
    object CDSConsClienteOSTEL1: TStringField
      FieldName = 'TEL1'
      Size = 15
    end
    object CDSConsClienteOSTEL2: TStringField
      FieldName = 'TEL2'
      Size = 15
    end
    object CDSConsClienteOSCELULAR: TStringField
      FieldName = 'CELULAR'
      Size = 15
    end
    object CDSConsClienteOSVALORMENSALIDADE: TFloatField
      FieldName = 'VALORMENSALIDADE'
    end
    object CDSConsClienteOSDATA_VENCIMENTO: TDateField
      FieldName = 'DATA_VENCIMENTO'
    end
    object CDSConsClienteOSSTATUS: TStringField
      FieldName = 'STATUS'
      Size = 12
    end
    object CDSConsClienteOSNASCIMENTO: TDateField
      FieldName = 'NASCIMENTO'
    end
    object CDSConsClienteOSDATA_INSTALACAO: TDateField
      FieldName = 'DATA_INSTALACAO'
    end
    object CDSConsClienteOSINADIMPLENCIA: TStringField
      FieldName = 'INADIMPLENCIA'
      Size = 100
    end
    object CDSConsClienteOSPARCELAS_GERADAS: TStringField
      FieldName = 'PARCELAS_GERADAS'
      Size = 1
    end
  end
  object FDConsTecnicosOS: TFDQuery
    Connection = FDConn
    SQL.Strings = (
      'SELECT * FROM TECNICOS')
    Left = 648
    Top = 352
  end
  object DSPConsTecnicosOS: TDataSetProvider
    DataSet = FDConsTecnicosOS
    Left = 768
    Top = 360
  end
  object CDSConsTecnicosOS: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DSPConsTecnicosOS'
    Left = 872
    Top = 352
    object CDSConsTecnicosOSID_TECNICO: TIntegerField
      FieldName = 'ID_TECNICO'
      Required = True
    end
    object CDSConsTecnicosOSNOME: TStringField
      FieldName = 'NOME'
      Size = 100
    end
    object CDSConsTecnicosOSCELULAR: TStringField
      FieldName = 'CELULAR'
      Size = 25
    end
  end
  object FDSummaryOS: TFDQuery
    Connection = FDConn
    SQL.Strings = (
      'SELECT'
      
        ' COUNT(CASE ORDEM_SERVICO.STATUS WHEN '#39'A'#39' THEN 1 ELSE NULL END) ' +
        'ABERTOS,'
      
        ' COUNT(CASE ORDEM_SERVICO.STATUS WHEN '#39'E'#39' THEN 1 ELSE NULL END) ' +
        'EM_EXECUCAO,'
      
        ' COUNT(CASE ORDEM_SERVICO.STATUS WHEN '#39'C'#39' THEN 1 ELSE NULL END) ' +
        'CONCLUIDOS,'
      ' COUNT(ORDEM_SERVICO.ID_ORDEM) AS TOTAL'
      ' FROM ORDEM_SERVICO'
      'WHERE'
      'ORDEM_SERVICO.DT_ABERTURA BETWEEN :DATA_INICIO AND :DATA_FIM')
    Left = 648
    Top = 416
    ParamData = <
      item
        Position = 1
        Name = 'DATA_INICIO'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Position = 2
        Name = 'DATA_FIM'
        DataType = ftDate
        ParamType = ptInput
      end>
  end
  object DSPSummaryOS: TDataSetProvider
    DataSet = FDSummaryOS
    Left = 768
    Top = 416
  end
  object CDSSummaryOS: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftDate
        Name = 'DATA_INICIO'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'DATA_FIM'
        ParamType = ptInput
      end>
    ProviderName = 'DSPSummaryOS'
    Left = 872
    Top = 416
    object CDSSummaryOSABERTOS: TIntegerField
      FieldName = 'ABERTOS'
      ReadOnly = True
    end
    object CDSSummaryOSEM_EXECUCAO: TIntegerField
      FieldName = 'EM_EXECUCAO'
      ReadOnly = True
    end
    object CDSSummaryOSCONCLUIDOS: TIntegerField
      FieldName = 'CONCLUIDOS'
      ReadOnly = True
    end
    object CDSSummaryOSTOTAL: TIntegerField
      FieldName = 'TOTAL'
      ReadOnly = True
    end
  end
  object FDConsUsuarios: TFDQuery
    Connection = FDConn
    SQL.Strings = (
      'SELECT * FROM USUARIOS'
      'ORDER BY ID_USUARIO')
    Left = 648
    Top = 496
  end
  object DSPConsUsuarios: TDataSetProvider
    DataSet = FDConsUsuarios
    Left = 768
    Top = 496
  end
  object CDSConsUsuarios: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DSPConsUsuarios'
    Left = 872
    Top = 496
    object CDSConsUsuariosID_USUARIO: TIntegerField
      FieldName = 'ID_USUARIO'
      Required = True
    end
    object CDSConsUsuariosNOME: TStringField
      FieldName = 'NOME'
      Size = 100
    end
    object CDSConsUsuariosSENHA: TStringField
      FieldName = 'SENHA'
      Size = 100
    end
  end
  object FDCadUsuario: TFDQuery
    Connection = FDConn
    SQL.Strings = (
      'SELECT * FROM USUARIOS WHERE ID_USUARIO = :ID_USUARIO')
    Left = 648
    Top = 552
    ParamData = <
      item
        Name = 'ID_USUARIO'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
  end
  object DSPCadUsuario: TDataSetProvider
    DataSet = FDCadUsuario
    Left = 768
    Top = 560
  end
  object CDSCadUsuario: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'ID_USUARIO'
        ParamType = ptInput
      end>
    ProviderName = 'DSPCadUsuario'
    Left = 872
    Top = 568
    object CDSCadUsuarioID_USUARIO: TIntegerField
      FieldName = 'ID_USUARIO'
      Required = True
    end
    object CDSCadUsuarioNOME: TStringField
      FieldName = 'NOME'
      Size = 100
    end
    object CDSCadUsuarioSENHA: TStringField
      FieldName = 'SENHA'
      Size = 100
    end
  end
  object FDConsTecnicos: TFDQuery
    Connection = FDConn
    SQL.Strings = (
      'SELECT * FROM TECNICOS'
      'ORDER BY ID_TECNICO')
    Left = 648
    Top = 624
  end
  object DSPConsTecnicos: TDataSetProvider
    DataSet = FDConsTecnicos
    Left = 768
    Top = 624
  end
  object CDSConsTecnicos: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DSPConsTecnicos'
    Left = 872
    Top = 632
    object CDSConsTecnicosID_TECNICO: TIntegerField
      FieldName = 'ID_TECNICO'
      Required = True
    end
    object CDSConsTecnicosNOME: TStringField
      FieldName = 'NOME'
      Size = 100
    end
    object CDSConsTecnicosCELULAR: TStringField
      FieldName = 'CELULAR'
      Size = 25
    end
  end
  object FDCadTecnico: TFDQuery
    Connection = FDConn
    SQL.Strings = (
      'SELECT * FROM TECNICOS WHERE ID_TECNICO = :ID_TECNICO')
    Left = 648
    Top = 696
    ParamData = <
      item
        Name = 'ID_TECNICO'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
  end
  object DSPCadTecnico: TDataSetProvider
    DataSet = FDCadTecnico
    Left = 768
    Top = 680
  end
  object CDSCadTecnico: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'ID_TECNICO'
        ParamType = ptInput
      end>
    ProviderName = 'DSPCadTecnico'
    Left = 872
    Top = 688
    object CDSCadTecnicoID_TECNICO: TIntegerField
      FieldName = 'ID_TECNICO'
      Required = True
    end
    object CDSCadTecnicoNOME: TStringField
      FieldName = 'NOME'
      Size = 100
    end
    object CDSCadTecnicoCELULAR: TStringField
      FieldName = 'CELULAR'
      Size = 25
    end
  end
  object FDClientesAtraso: TFDQuery
    Connection = FDConn
    SQL.Strings = (
      ' SELECT'
      '       DISTINCT(ID_CLIENTE)'
      '     FROM'
      '       PARCELAS'
      '    WHERE'
      
        '       PARCELAS.DATA_VENCIMENTO <=  CAST (CURRENT_DATE - 60 AS D' +
        'ATE ) AND STATUS = '#39'A'#39
      '')
    Left = 96
    Top = 696
  end
  object DSPClientesAtraso: TDataSetProvider
    DataSet = FDClientesAtraso
    Left = 200
    Top = 696
  end
  object CDSClientesAtraso: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DSPClientesAtraso'
    Left = 304
    Top = 696
    object CDSClientesAtrasoID_CLIENTE: TIntegerField
      FieldName = 'ID_CLIENTE'
    end
  end
  object FDLogin: TFDQuery
    Connection = FDConn
    SQL.Strings = (
      'SELECT * FROM USUARIOS'
      'WHERE USUARIOS.NOME = :LOGIN'
      'AND USUARIOS.SENHA = :SENHA')
    Left = 408
    Top = 72
    ParamData = <
      item
        Name = 'LOGIN'
        DataType = ftString
        ParamType = ptInput
        Size = 100
        Value = Null
      end
      item
        Name = 'SENHA'
        DataType = ftString
        ParamType = ptInput
        Size = 100
      end>
  end
  object DSPLogin: TDataSetProvider
    DataSet = FDLogin
    Left = 488
    Top = 72
  end
  object CDSLogin: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftString
        Name = 'LOGIN'
        ParamType = ptInput
        Size = 100
      end
      item
        DataType = ftString
        Name = 'SENHA'
        ParamType = ptInput
        Size = 100
      end>
    ProviderName = 'DSPLogin'
    Left = 560
    Top = 72
    object CDSLoginID_USUARIO: TIntegerField
      FieldName = 'ID_USUARIO'
      Required = True
    end
    object CDSLoginNOME: TStringField
      FieldName = 'NOME'
      Size = 100
    end
    object CDSLoginSENHA: TStringField
      FieldName = 'SENHA'
      Size = 100
    end
  end
  object FDRelParcelasPagas: TFDQuery
    Connection = FDConn
    SQL.Strings = (
      'SELECT'
      'CLIENTES.NOME,'
      'PARCELAS.DATA_VENCIMENTO,'
      'PARCELAS.DATA_PAGAMENTO,'
      'PARCELAS.VALOR'
      'FROM PARCELAS'
      
        'INNER JOIN CLIENTES ON (PARCELAS.ID_CLIENTE = CLIENTES.ID_CLIENT' +
        'E)'
      
        'WHERE PARCELAS.DATA_PAGAMENTO BETWEEN :DATA_INICIO AND :DATA_FIM' +
        ' AND PARCELAS.STATUS = '#39'P'#39)
    Left = 400
    Top = 152
    ParamData = <
      item
        Position = 1
        Name = 'DATA_INICIO'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Position = 2
        Name = 'DATA_FIM'
        DataType = ftDate
        ParamType = ptInput
      end>
  end
  object DSPRelParcelasPagas: TDataSetProvider
    DataSet = FDRelParcelasPagas
    Left = 488
    Top = 152
  end
  object CDSRelParcelasPagas: TClientDataSet
    Aggregates = <>
    AggregatesActive = True
    Params = <
      item
        DataType = ftDate
        Name = 'DATA_INICIO'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'DATA_FIM'
        ParamType = ptInput
      end>
    ProviderName = 'DSPRelParcelasPagas'
    Left = 560
    Top = 152
    object CDSRelParcelasPagasNOME: TStringField
      FieldName = 'NOME'
      Origin = 'NOME'
      Size = 100
    end
    object CDSRelParcelasPagasDATA_VENCIMENTO: TDateField
      FieldName = 'DATA_VENCIMENTO'
      Origin = 'DATA_VENCIMENTO'
    end
    object CDSRelParcelasPagasDATA_PAGAMENTO: TDateField
      FieldName = 'DATA_PAGAMENTO'
      Origin = 'DATA_PAGAMENTO'
    end
    object CDSRelParcelasPagasVALOR: TFloatField
      FieldName = 'VALOR'
      Origin = 'VALOR'
      currency = True
    end
    object CDSRelParcelasPagasSOMA: TAggregateField
      FieldName = 'SOMA'
      Active = True
      currency = True
      DisplayName = ''
      Expression = 'SUM(VALOR)'
    end
  end
  object FDConsOSID: TFDQuery
    Connection = FDConn
    SQL.Strings = (
      'SELECT'
      '    ORDEM_SERVICO.*,'
      '    CLIENTES.NOME,'
      '    CLIENTES.ENDERECO,'
      '    CLIENTES.BAIRRO,'
      '    CLIENTES.REFERENCIA,'
      '    CLIENTES.TEL1,'
      '    CLIENTES.CELULAR,'
      '    TECNICOS.ID_TECNICO,'
      '    TECNICOS.NOME AS TECNICO,'
      '    TECNICOS.CELULAR'
      ' FROM ORDEM_SERVICO'
      
        'INNER JOIN CLIENTES ON (ORDEM_SERVICO.ID_CLIENTE = CLIENTES.ID_C' +
        'LIENTE)'
      
        'INNER JOIN TECNICOS ON (ORDEM_SERVICO.ID_TECNICO = TECNICOS.ID_T' +
        'ECNICO)'
      'WHERE'
      'ORDEM_SERVICO.ID_ORDEM = :ID_ORDEM'
      ''
      '')
    Left = 400
    Top = 216
    ParamData = <
      item
        Name = 'ID_ORDEM'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
  end
  object DSPConsOSID: TDataSetProvider
    DataSet = FDConsOSID
    Left = 488
    Top = 216
  end
  object CDSConsOSID: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'ID_ORDEM'
        ParamType = ptInput
      end>
    ProviderName = 'DSPConsOSID'
    Left = 560
    Top = 216
    object IntegerField1: TIntegerField
      FieldName = 'ID_ORDEM'
      Required = True
    end
    object IntegerField2: TIntegerField
      FieldName = 'ID_CLIENTE'
    end
    object IntegerField3: TIntegerField
      FieldName = 'ID_TECNICO'
    end
    object StringField1: TStringField
      FieldName = 'TIPO'
      Size = 50
    end
    object StringField2: TStringField
      FieldName = 'PROBLEMA'
      Size = 150
    end
    object DateField1: TDateField
      FieldName = 'DT_ABERTURA'
    end
    object TimeField1: TTimeField
      FieldName = 'HR_ABERTURA'
    end
    object DateField2: TDateField
      FieldName = 'DT_FINAL'
    end
    object TimeField2: TTimeField
      FieldName = 'HR_FINAL'
    end
    object StringField3: TStringField
      FieldName = 'SOLUCAO_ADOTADA'
      Size = 500
    end
    object StringField4: TStringField
      FieldName = 'OBSERVACAO'
      Size = 500
    end
    object DateField3: TDateField
      FieldName = 'DT_FECHAMENTO'
    end
    object TimeField3: TTimeField
      FieldName = 'HR_FECHAMENTO'
    end
    object StringField5: TStringField
      FieldName = 'STATUS'
      Size = 1
    end
    object StringField6: TStringField
      FieldName = 'NOME'
      ReadOnly = True
      Size = 100
    end
    object StringField7: TStringField
      FieldName = 'ENDERECO'
      ReadOnly = True
      Size = 50
    end
    object StringField8: TStringField
      FieldName = 'BAIRRO'
      ReadOnly = True
    end
    object StringField9: TStringField
      FieldName = 'REFERENCIA'
      ReadOnly = True
      Size = 500
    end
    object StringField10: TStringField
      FieldName = 'TEL1'
      ReadOnly = True
      Size = 15
    end
    object StringField11: TStringField
      FieldName = 'CELULAR'
      ReadOnly = True
      Size = 15
    end
    object IntegerField4: TIntegerField
      FieldName = 'ID_TECNICO_1'
      ReadOnly = True
    end
    object StringField12: TStringField
      FieldName = 'TECNICO'
      ReadOnly = True
      Size = 100
    end
    object StringField13: TStringField
      FieldName = 'CELULAR_1'
      ReadOnly = True
      Size = 25
    end
  end
  object FDReciboTodos: TFDQuery
    Connection = FDConn
    SQL.Strings = (
      'SELECT'
      '    PARCELAS.ID_CLIENTE,'
      '    CLIENTES.NOME,'
      '    CLIENTES.BAIRRO,'
      '    CLIENTES.ENDERECO,'
      '    PARCELAS.DATA_VENCIMENTO,'
      '    PARCELAS.VALOR,'
      '    PARCELAS.STATUS,'
      '    CONFIG.MENSAGEM_CARNE,'
      '    CONFIG.VALOR_MENSALIDADE,'
      '    CONFIG.VALOR_DESCONTO'
      'FROM'
      '    PARCELAS, CONFIG'
      
        'INNER JOIN CLIENTES ON (PARCELAS.ID_CLIENTE = CLIENTES.ID_CLIENT' +
        'E)'
      'WHERE PARCELAS.ID_CLIENTE = :ID_CLIENTE')
    Left = 392
    Top = 288
    ParamData = <
      item
        Name = 'ID_CLIENTE'
        ParamType = ptInput
        Value = Null
      end>
  end
  object DSPReciboTodos: TDataSetProvider
    DataSet = FDReciboTodos
    Left = 488
    Top = 288
  end
  object CDSReciboTodos: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DSPReciboTodos'
    Left = 560
    Top = 288
  end
end
