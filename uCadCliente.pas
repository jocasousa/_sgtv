unit uCadCliente;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.DBCtrls,
  Vcl.Mask, Vcl.ComCtrls, Data.DB, Vcl.Imaging.pngimage;

type
  TfrmCadCliente = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Panel4: TPanel;
    DSCadCliente: TDataSource;
    Label24: TLabel;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    DTnascimento: TDateTimePicker;
    DTInstala��o: TDateTimePicker;
    EdtTelefone1: TMaskEdit;
    EdtTelefone2: TMaskEdit;
    EdtCelular: TMaskEdit;
    CBStatus: TComboBox;
    GroupBox2: TGroupBox;
    Label12: TLabel;
    DBEdit6: TDBEdit;
    Label13: TLabel;
    DBEdit7: TDBEdit;
    Label14: TLabel;
    DBEdit8: TDBEdit;
    Label15: TLabel;
    DBEdit9: TDBEdit;
    Label16: TLabel;
    DBEdit10: TDBEdit;
    Label17: TLabel;
    EdtCep: TMaskEdit;
    Label18: TLabel;
    DBEdit11: TDBEdit;
    Label19: TLabel;
    CBInadimplencia: TComboBox;
    DBMemo1: TDBMemo;
    Label20: TLabel;
    Panel3: TPanel;
    BtnSalvar: TImage;
    BtnCancelar: TImage;
    EdtCodigo: TEdit;
    lbmodoVisualizacao: TLabel;
    procedure BtnSalvarClick(Sender: TObject);
    procedure BtnCancelarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
  public
  var NovoCadastro : Boolean;
    { Public declarations }
  end;

var
  frmCadCliente: TfrmCadCliente;

implementation

{$R *.dfm}

uses uDM, uFuncoes;

procedure TfrmCadCliente.FormCreate(Sender: TObject);
begin
  DTnascimento.Date := Now;
  DTInstala��o.Date := Now;
end;

procedure TfrmCadCliente.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_ESCAPE then
  begin
    DM.CDSCadCliente.Cancel;
    Close;
  end;
end;

procedure TfrmCadCliente.BtnSalvarClick(Sender: TObject);
var I : Integer;
DataVencimento, DataConv : TDateTime;
Dia, Mes, Ano, AnoAtual, MesAtual, DiaAtual : Word;
begin

   try
    if (DM.CDSCadClienteID_CLIENTE.Value = 0) and (EdtCodigo.Text = '') then
    begin
      DM.CDSCadClienteID_CLIENTE.Value :=  uFuncoes.RetornaId('ID_CLIENTE');
    end
    else
    begin
     DM.CDSCadClienteID_CLIENTE.Value := StrToInt(EdtCodigo.Text);
    end;

     DM.CDSCadClienteNASCIMENTO.Value := DTnascimento.Date;
     DM.CDSCadClienteDATA_INSTALACAO.Value := DTInstala��o.Date;
     DM.CDSCadClienteTEL1.Value := EdtTelefone1.Text;
     DM.CDSCadClienteTEL2.Value := EdtTelefone2.Text;
     DM.CDSCadClienteCELULAR.Value := EdtCelular.Text;
     DM.CDSCadClienteSTATUS.Value  :=  CBStatus.Text;
     DM.CDSCadClienteCEP.Value     := EdtCep.Text;
     DM.CDSCadClienteINADIMPLENCIA.Value := CBInadimplencia.Text;

     //Gera��o de Parcela
     if NovoCadastro then
     begin

       DM.CDSCadParcela.Close;
       DM.CDSCadParcela.ParamByName('ID_CLIENTE').Value := DM.CDSCadClienteID_CLIENTE.Value;
       DM.CDSCadParcela.Open;

       DM.CDSConfig.Close;
       DM.CDSConfig.Open;

       if DM.CDSCadParcela.RecordCount = 0 then
       begin

         DataVencimento := DTInstala��o.Date;
         DecodeDate(DataVencimento, Ano, Mes, Dia);
         DecodeDate(Now, AnoAtual, MesAtual, DiaAtual);

         //Gera 12 Parcelas
         for I := 1 to 12 do
         begin

           if (MesAtual = Mes) and (Dia < 15)  then
           begin
             DataVencimento := DataVencimento + (15 - Dia);
             DecodeDate(DataVencimento, Ano, Mes, Dia);
             DataConv  := EncodeDate(Ano, Mes, 15);
           end
           else
           begin
            DataVencimento := DataVencimento + 30;
            DecodeDate(DataVencimento, Ano, Mes, Dia);
            DataConv  := EncodeDate(Ano, Mes, 15);
           end;


           DM.CDSCadParcela.Append;
           DM.CDSCadParcelaID_PARCELA.Value := uFuncoes.RetornaId('ID_PARCELA');
           DM.CDSCadParcelaID_CLIENTE.Value := DM.CDSCadClienteID_CLIENTE.Value;
           DM.CDSCadParcelaVALOR.Value      := DM.CDSConfigVALOR_MENSALIDADE.Value;
           DM.CDSCadParcelaNUM_PARCELA.Value:= I;
           DM.CDSCadParcelaDATA_VENCIMENTO.Value := DataConv;
           DM.CDSCadParcelaSTATUS.Value     := 'A';

           DM.CDSCadParcela.Post;
         end;

         if DM.CDSCadParcela.ApplyUpdates(0) = 0 then
         begin
            DM.CDSCadClientePARCELAS_GERADAS.Value := 'S';
         end;

       end;

     end;


     DM.CDSCadCliente.Post;
     DM.CDSCadCliente.ApplyUpdates(0);

     ShowMessage('Cadastro Realizado Com Sucesso!');

     DM.CDSConsCliente.Close;
     DM.CDSConsCliente.Open;
     Close;
   except on E: Exception do
      ShowMessage(E.Message);
   end;
end;

procedure TfrmCadCliente.BtnCancelarClick(Sender: TObject);
begin
  DM.CDSCadCliente.Cancel;
  Close;
end;

end.
