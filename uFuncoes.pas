unit uFuncoes;


interface

uses  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.Grids, Vcl.DBGrids,
  Vcl.ComCtrls, Vcl.ExtCtrls, Vcl.StdCtrls, Vcl.WinXCtrls, Vcl.DBCtrls,
  Vcl.Imaging.pngimage, System.Math;

   procedure DimensionarGrid(dbg: TDBGrid);
   procedure ExibeMensagem(Texto : String; Exibe : Boolean);
   function Crypt(Action, Src: String): String;
   function RetornaId ( Campo : String ) : Integer;

implementation

uses uDM;


function RetornaId(Campo: String): Integer;
begin
  DM.CDSControle.Close;
  DM.CDSControle.ParamByName('CAMPO').Value := Campo;
  DM.CDSControle.Open;

  if DM.CDSControle.Locate('CAMPO', Campo, []) then
  begin
    DM.CDSControle.Close;
    DM.CDSControle.ParamByName('CAMPO').Value := Campo;
    DM.CDSControle.Open;
    DM.CDSControle.Edit;

    DM.CDSControleVALOR.Value := DM.CDSControleVALOR.Value + 1;

    DM.CDSControle.Post;
    DM.CDSControle.ApplyUpdates(0);

   if DM.CDSControle.ApplyUpdates(0) = 0 then
      Result := DM.CDSControleVALOR.AsInteger
    else
      RetornaId(Campo);

    DM.FDConn.Connected := False;
    DM.FDConn.Connected := True;
  end;

end;




function Crypt(Action, Src: String): String;
var KeyLen : Integer;
  KeyPos : Integer;
  OffSet : Integer;
  Dest, Key : String;
  SrcPos : Integer;
  SrcAsc : Integer;
  TmpSrcAsc : Integer;
  Range : Integer;
begin
  if (Src = '') Then
  begin
    Result:= '';
    Exit;
  end;
  Key :=
'YUQL23KL23DF90WI5E1JAS467NMCXXL6JAOAUWWMCL0AOMM4A4VZYW9KHJUI2347EJHJKDF3424SKL K3LAKDJSL9RTIKJ';
  Dest := '';
  KeyLen := Length(Key);
  KeyPos := 0;
  SrcPos := 0;
  SrcAsc := 0;
  Range := 256;
  if (Action = UpperCase('C')) then
  begin
    Randomize;
    OffSet := Random(Range);
    Dest := Format('%1.2x',[OffSet]);
    for SrcPos := 1 to Length(Src) do
    begin
      Application.ProcessMessages;
      SrcAsc := (Ord(Src[SrcPos]) + OffSet) Mod 255;
      if KeyPos < KeyLen then KeyPos := KeyPos + 1 else KeyPos := 1;
      SrcAsc := SrcAsc Xor Ord(Key[KeyPos]);
      Dest := Dest + Format('%1.2x',[SrcAsc]);
      OffSet := SrcAsc;
    end;
  end
  Else if (Action = UpperCase('D')) then
  begin
    OffSet := StrToIntDef('$'+ copy(Src,1,2),0);
    SrcPos := 3;
  repeat
    SrcAsc := StrToIntDef('$'+ copy(Src,SrcPos,2),0);
    if (KeyPos < KeyLen) Then KeyPos := KeyPos + 1 else KeyPos := 1;
    TmpSrcAsc := SrcAsc Xor Ord(Key[KeyPos]);
    if TmpSrcAsc <= OffSet then TmpSrcAsc := 255 + TmpSrcAsc - OffSet
    else TmpSrcAsc := TmpSrcAsc - OffSet;
    Dest := Dest + Chr(TmpSrcAsc);
    OffSet := SrcAsc;
    SrcPos := SrcPos + 2;
  until (SrcPos >= Length(Src));
  end;
  Result:= Dest;
end;

procedure ExibeMensagem(Texto : String; Exibe : Boolean);
begin
//  Application.ProcessMessages;
//  if Exibe then
//   begin
//     FrmMensagem.LabelMensagem.Caption := Texto;
//     FrmMensagem.Width := FrmMensagem.LabelMensagem.Width + 300;
//     FrmMensagem.Left := StrtoInt(FloattoStr(Int((Screen.Width / 2) - (FrmMensagem.Width / 2))));
//     Screen.Cursor := crHourGlass;
//     FrmMensagem.Show;
//   end
//  else
//   begin
//     Screen.Cursor := crDefault;
//     FrmMensagem.ModalResult := mrOk;
//     FrmMensagem.Close;
//   end;
//  Application.ProcessMessages;
//  SetForegroundWindow(Application.Handle);
end;




procedure DimensionarGrid(dbg: TDBGrid);
type
  TArray = Array of Integer;
  procedure AjustarColumns(Swidth, TSize: Integer; Asize: TArray);
  var
    idx: Integer;
  begin
    if TSize = 0 then
    begin
      TSize := dbg.Columns.count;
      for idx := 0 to dbg.Columns.count - 1 do
        dbg.Columns[idx].Width := (dbg.Width - dbg.Canvas.TextWidth('AAAAAA')
          ) div TSize
    end
    else
      for idx := 0 to dbg.Columns.count - 1 do
        dbg.Columns[idx].Width := dbg.Columns[idx].Width +
          (Swidth * Asize[idx] div TSize);
  end;

var
  idx, Twidth, TSize, Swidth: Integer;
  AWidth: TArray;
  Asize: TArray;
  NomeColuna: String;
begin
  SetLength(AWidth, dbg.Columns.count);
  SetLength(Asize, dbg.Columns.count);
  Twidth := 0;
  TSize := 0;



  for idx := 0 to dbg.Columns.count - 1 do
  begin


    NomeColuna := dbg.Columns[idx].Title.Caption;
    dbg.Columns[idx].Width := dbg.Canvas.TextWidth
      (dbg.Columns[idx].Title.Caption + 'AAA');
    AWidth[idx] := dbg.Columns[idx].Width;
    Twidth := Twidth + AWidth[idx];

    if Assigned(dbg.Columns[idx].Field) then
      Asize[idx] := dbg.Columns[idx].Field.Size
    else
      Asize[idx] := 1;

    TSize := TSize + Asize[idx];
  end;
  if TDBGridOption.dgColLines in dbg.Options then
    Twidth := Twidth + dbg.Columns.count;

  // adiciona a largura da coluna indicada do cursor
  if TDBGridOption.dgIndicator in dbg.Options then
    Twidth := Twidth + IndicatorWidth;

  Swidth := dbg.ClientWidth - Twidth;
  AjustarColumns(Swidth, TSize, Asize);

end;






end.

