unit uPrincipal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls,
  Vcl.Imaging.pngimage, Vcl.Menus;

type
  TfrmPrincipal = class(TForm)
    pTop: TPanel;
    pBottom: TPanel;
    pCenter: TPanel;
    Image1: TImage;
    Image2: TImage;
    Image3: TImage;
    Image4: TImage;
    MainMenu1: TMainMenu;
    Cadastros1: TMenuItem;
    cnicos1: TMenuItem;
    Usurios1: TMenuItem;
    Usurios2: TMenuItem;
    BloqueiaClientesAtraso1: TMenuItem;
    Config1: TMenuItem;
    procedure Image2Click(Sender: TObject);
    procedure Image3Click(Sender: TObject);
    procedure Image4Click(Sender: TObject);
    procedure Usurios1Click(Sender: TObject);
    procedure cnicos1Click(Sender: TObject);
    procedure BloqueiaClientesAtraso1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Config1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPrincipal: TfrmPrincipal;

implementation

{$R *.dfm}

uses uConsCliente, uDM, uParcelas, uOrdemServico, uConsUsuarios, uCadTecnicos,
  uConsTecnicos, uLogin, uConfig;

procedure TfrmPrincipal.BloqueiaClientesAtraso1Click(Sender: TObject);
begin
  //DM.CDSConfig.Close;
  //DM.CDSConfig.Open;

  DM.CDSClientesAtraso.Close;
  //DM.FDClientesAtraso.SQL.Add('SELECT DISTINCT(ID_CLIENTE) FROM PARCELAS  WHERE  PARCELAS.DATA_VENCIMENTO <=  CAST (CURRENT_DATE - ' +  DM.CDSConfigDIAS_BLOQUEIO.AsString   + ' AS DATE ) AND STATUS = ''A''');
  DM.CDSClientesAtraso.Open;
  DM.CDSClientesAtraso.First;

  if DM.CDSClientesAtraso.RecordCount > 0 then
  begin

    while not DM.CDSClientesAtraso.Eof do
    begin
       DM.CDSCadCliente.Close;
       DM.CDSCadCliente.ParamByName('ID_CLIENTE').Value := DM.CDSClientesAtrasoID_CLIENTE.Value;
       DM.CDSCadCliente.Open;

       if DM.CDSCadCliente.RecordCount > 0 then
       begin
         DM.CDSCadCliente.Edit;
         DM.CDSCadClienteSTATUS.Value := 'B';
         DM.CDSCadCliente.Post;
         DM.CDSCadCliente.ApplyUpdates(0);
       end;

       DM.CDSClientesAtraso.Next;
    end;

    ShowMessage('Bloquados ' + DM.CDSClientesAtraso.RecordCount.ToString + ' Clientes por Atraso.');
  end
  else
  begin
    ShowMessage('N�o existe nenhum cliente para ser bloqueado por atraso.');
  end;

end;

procedure TfrmPrincipal.cnicos1Click(Sender: TObject);
begin
  try
    Application.CreateForm(TFrmConsTecnicos, FrmConsTecnicos);
    FrmConsTecnicos.ShowModal;
  finally
    FrmConsTecnicos.Free;
  end;
end;

procedure TfrmPrincipal.Config1Click(Sender: TObject);
begin
 try
   Application.CreateForm(TFrmConfig, FrmConfig);
   DM.CDSConfig.Close;
   DM.CDSConfig.Open;
   DM.CDSConfig.Edit;
   FrmConfig.ShowModal;
 finally
   FrmConfig.Free;
 end;
end;

procedure TfrmPrincipal.FormShow(Sender: TObject);
begin
 frmLogin.ShowModal;
end;

procedure TfrmPrincipal.Image2Click(Sender: TObject);
begin
  try
       Application.CreateForm(TfrmConsCliente, frmConsCliente);
       DM.CDSConsCliente.Close;
       DM.CDSConsCliente.Open;
       frmConsCliente.ShowModal;
    finally
       frmConsCliente.Free;
    end;
end;

procedure TfrmPrincipal.Image3Click(Sender: TObject);
begin
 try
   Application.CreateForm(TfrmParcelas, frmParcelas);
   frmParcelas.ShowModal;
 finally
 frmParcelas.Free;
 end;
end;

procedure TfrmPrincipal.Image4Click(Sender: TObject);
begin
 try
   Application.CreateForm(TfrmOrdemServico, frmOrdemServico);
   frmOrdemServico.PesquisaOS;
   frmOrdemServico.ShowModal;
 finally
  frmOrdemServico.Free;
 end;
end;

procedure TfrmPrincipal.Usurios1Click(Sender: TObject);
begin
  try
    Application.CreateForm(TFrmConsUsuarios, FrmConsUsuarios);
    FrmConsUsuarios.ShowModal;
  finally
    FrmConsUsuarios.Free;
  end;
end;

end.
