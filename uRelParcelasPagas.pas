unit uRelParcelasPagas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Imaging.pngimage, Vcl.ExtCtrls,
  Vcl.ComCtrls, Data.DB, frxClass, frxDBSet;

type
  TFrmRelParcelasPagas = class(TForm)
    DT1: TDateTimePicker;
    DT2: TDateTimePicker;
    BtnImprimir: TImage;
    frxDBRelParcelasPagas: TfrxDBDataset;
    frxRpRelParcelasPagas: TfrxReport;
    DSRelParcelasPagas: TDataSource;
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure BtnImprimirClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmRelParcelasPagas: TFrmRelParcelasPagas;

implementation

{$R *.dfm}

uses uDM;

procedure TFrmRelParcelasPagas.BtnImprimirClick(Sender: TObject);
begin
 DM.CDSRelParcelasPagas.Close;
 DM.CDSRelParcelasPagas.ParamByName('DATA_INICIO').Value := DT1.Date;
 DM.CDSRelParcelasPagas.ParamByName('DATA_FIM').Value := DT2.Date;
 DM.CDSRelParcelasPagas.Open;

 if DM.CDSRelParcelasPagas.RecordCount > 0  then
 begin
   frxRpRelParcelasPagas.PrepareReport(True);
   frxRpRelParcelasPagas.ShowReport;
 end
 else
 begin
   ShowMessage('N�o existem parcelas pagas nesse periodo.');
 end;
end;

procedure TFrmRelParcelasPagas.FormCreate(Sender: TObject);
begin
  DT1.Date := Now;
  DT2.Date := Now;
end;

procedure TFrmRelParcelasPagas.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if Key = VK_ESCAPE then
 begin
   Close;
 end;
end;

end.
