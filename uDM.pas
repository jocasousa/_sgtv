unit uDM;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.UI.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Phys, FireDAC.VCLUI.Wait,
  FireDAC.Phys.FBDef, FireDAC.Phys.IBBase, FireDAC.Phys.FB, FireDAC.Comp.UI,
  Data.DB, FireDAC.Comp.Client, FireDAC.Comp.DataSet, MidasLib,
  Datasnap.DBClient, Datasnap.Provider, IniFiles;

type
  TDM = class(TDataModule)
    FDConsCliente: TFDQuery;
    FDConn: TFDConnection;
    FDGUIxWaitCursor1: TFDGUIxWaitCursor;
    FDPhysFBDriverLink1: TFDPhysFBDriverLink;
    DSPConsCliente: TDataSetProvider;
    CDSConsCliente: TClientDataSet;
    FDCadCliente: TFDQuery;
    DSPCadCliente: TDataSetProvider;
    CDSCadCliente: TClientDataSet;
    FDControle: TFDQuery;
    DSPControle: TDataSetProvider;
    CDSControle: TClientDataSet;
    CDSControleCAMPO: TStringField;
    CDSControleVALOR: TIntegerField;
    CDSCadClienteID_CLIENTE: TIntegerField;
    CDSCadClienteNOME: TStringField;
    CDSCadClienteENDERECO: TStringField;
    CDSCadClienteNUMERO: TStringField;
    CDSCadClienteBAIRRO: TStringField;
    CDSCadClienteCIDADE: TStringField;
    CDSCadClienteUF: TStringField;
    CDSCadClienteCEP: TStringField;
    CDSCadClienteREFERENCIA: TStringField;
    CDSCadClienteCPF_CNPJ: TStringField;
    CDSCadClienteRG_IE: TStringField;
    CDSCadClienteEMAIL: TStringField;
    CDSCadClienteVENCIMENTO: TStringField;
    CDSCadClienteOBSERVACAO: TStringField;
    CDSCadClienteTEL1: TStringField;
    CDSCadClienteTEL2: TStringField;
    CDSCadClienteCELULAR: TStringField;
    CDSCadClienteVALORMENSALIDADE: TFloatField;
    CDSCadClienteDATA_VENCIMENTO: TDateField;
    CDSCadClienteSTATUS: TStringField;
    CDSCadClienteNASCIMENTO: TDateField;
    CDSCadClienteDATA_INSTALACAO: TDateField;
    CDSCadClienteINADIMPLENCIA: TStringField;
    CDSConsClienteID_CLIENTE: TIntegerField;
    CDSConsClienteNOME: TStringField;
    CDSConsClienteENDERECO: TStringField;
    CDSConsClienteNUMERO: TStringField;
    CDSConsClienteBAIRRO: TStringField;
    CDSConsClienteCIDADE: TStringField;
    CDSConsClienteUF: TStringField;
    CDSConsClienteCEP: TStringField;
    CDSConsClienteREFERENCIA: TStringField;
    CDSConsClienteCPF_CNPJ: TStringField;
    CDSConsClienteRG_IE: TStringField;
    CDSConsClienteEMAIL: TStringField;
    CDSConsClienteVENCIMENTO: TStringField;
    CDSConsClienteOBSERVACAO: TStringField;
    CDSConsClienteTEL1: TStringField;
    CDSConsClienteTEL2: TStringField;
    CDSConsClienteCELULAR: TStringField;
    CDSConsClienteVALORMENSALIDADE: TFloatField;
    CDSConsClienteDATA_VENCIMENTO: TDateField;
    CDSConsClienteSTATUS: TStringField;
    CDSConsClienteNASCIMENTO: TDateField;
    CDSConsClienteDATA_INSTALACAO: TDateField;
    CDSConsClienteINADIMPLENCIA: TStringField;
    FDConsParcelas: TFDQuery;
    DSPConsParcelas: TDataSetProvider;
    CDSConsParcelas: TClientDataSet;
    CDSConsParcelasID_PARCELA: TIntegerField;
    CDSConsParcelasID_CLIENTE: TIntegerField;
    CDSConsParcelasVALOR: TFloatField;
    CDSConsParcelasNUM_PARCELA: TIntegerField;
    CDSConsParcelasDATA_PAGAMENTO: TDateField;
    CDSConsParcelasDESCONTO: TStringField;
    CDSConsParcelasOBSERVACAO: TStringField;
    CDSConsParcelasSTATUS: TStringField;
    FDConsClienteParcela: TFDQuery;
    DSPConsClienteParcela: TDataSetProvider;
    CDSConsClienteParcela: TClientDataSet;
    CDSConsClienteParcelaID_CLIENTE: TIntegerField;
    CDSConsClienteParcelaNOME: TStringField;
    CDSConsClienteParcelaENDERECO: TStringField;
    CDSConsClienteParcelaNUMERO: TStringField;
    CDSConsClienteParcelaBAIRRO: TStringField;
    CDSConsClienteParcelaCIDADE: TStringField;
    CDSConsClienteParcelaUF: TStringField;
    CDSConsClienteParcelaCEP: TStringField;
    CDSConsClienteParcelaREFERENCIA: TStringField;
    CDSConsClienteParcelaCPF_CNPJ: TStringField;
    CDSConsClienteParcelaRG_IE: TStringField;
    CDSConsClienteParcelaEMAIL: TStringField;
    CDSConsClienteParcelaVENCIMENTO: TStringField;
    CDSConsClienteParcelaOBSERVACAO: TStringField;
    CDSConsClienteParcelaTEL1: TStringField;
    CDSConsClienteParcelaTEL2: TStringField;
    CDSConsClienteParcelaCELULAR: TStringField;
    CDSConsClienteParcelaVALORMENSALIDADE: TFloatField;
    CDSConsClienteParcelaDATA_VENCIMENTO: TDateField;
    CDSConsClienteParcelaSTATUS: TStringField;
    CDSConsClienteParcelaNASCIMENTO: TDateField;
    CDSConsClienteParcelaDATA_INSTALACAO: TDateField;
    CDSConsClienteParcelaINADIMPLENCIA: TStringField;
    CDSConsClientePARCELAS_GERADAS: TStringField;
    CDSCadClientePARCELAS_GERADAS: TStringField;
    CDSConsClienteParcelaPARCELAS_GERADAS: TStringField;
    FDCadParcela: TFDQuery;
    DSPCadParcela: TDataSetProvider;
    CDSCadParcela: TClientDataSet;
    CDSCadParcelaID_PARCELA: TIntegerField;
    CDSCadParcelaID_CLIENTE: TIntegerField;
    CDSCadParcelaVALOR: TFloatField;
    CDSCadParcelaNUM_PARCELA: TIntegerField;
    CDSCadParcelaDATA_PAGAMENTO: TDateField;
    CDSCadParcelaDESCONTO: TStringField;
    CDSCadParcelaOBSERVACAO: TStringField;
    CDSCadParcelaSTATUS: TStringField;
    FDConfig: TFDQuery;
    DSPConfig: TDataSetProvider;
    CDSConfig: TClientDataSet;
    CDSConfigVALOR_MENSALIDADE: TFloatField;
    CDSConfigVALOR_DESCONTO: TFloatField;
    CDSConsParcelasDATA_VENCIMENTO: TDateField;
    CDSCadParcelaDATA_VENCIMENTO: TDateField;
    FDEditParcela: TFDQuery;
    DSPEditParcela: TDataSetProvider;
    CDSEditParcela: TClientDataSet;
    CDSEditParcelaID_PARCELA: TIntegerField;
    CDSEditParcelaID_CLIENTE: TIntegerField;
    CDSEditParcelaVALOR: TFloatField;
    CDSEditParcelaNUM_PARCELA: TIntegerField;
    CDSEditParcelaDATA_VENCIMENTO: TDateField;
    CDSEditParcelaDATA_PAGAMENTO: TDateField;
    CDSEditParcelaDESCONTO: TStringField;
    CDSEditParcelaOBSERVACAO: TStringField;
    CDSEditParcelaSTATUS: TStringField;
    FDRecibo: TFDQuery;
    DSPRecibo: TDataSetProvider;
    CDSRecibo: TClientDataSet;
    CDSReciboID_CLIENTE: TIntegerField;
    CDSReciboNOME: TStringField;
    CDSReciboBAIRRO: TStringField;
    CDSReciboENDERECO: TStringField;
    CDSReciboDATA_VENCIMENTO: TDateField;
    CDSReciboVALOR: TFloatField;
    CDSReciboMENSAGEM_CARNE: TStringField;
    CDSReciboVALOR_MENSALIDADE: TFloatField;
    CDSReciboVALOR_DESCONTO: TFloatField;
    FDConsOS: TFDQuery;
    DSPConsOS: TDataSetProvider;
    CDSConsOS: TClientDataSet;
    FDCadOS: TFDQuery;
    DSPCadOS: TDataSetProvider;
    CDSCadOS: TClientDataSet;
    FDConsClienteOS: TFDQuery;
    DSPConsClienteOS: TDataSetProvider;
    CDSConsClienteOS: TClientDataSet;
    FDConsTecnicosOS: TFDQuery;
    DSPConsTecnicosOS: TDataSetProvider;
    CDSConsTecnicosOS: TClientDataSet;
    CDSConsTecnicosOSID_TECNICO: TIntegerField;
    CDSConsTecnicosOSNOME: TStringField;
    FDSummaryOS: TFDQuery;
    DSPSummaryOS: TDataSetProvider;
    CDSSummaryOS: TClientDataSet;
    CDSConsTecnicosOSCELULAR: TStringField;
    CDSConsOSID_ORDEM: TIntegerField;
    CDSConsOSID_CLIENTE: TIntegerField;
    CDSConsOSID_TECNICO: TIntegerField;
    CDSConsOSTIPO: TStringField;
    CDSConsOSPROBLEMA: TStringField;
    CDSConsOSDT_ABERTURA: TDateField;
    CDSConsOSHR_ABERTURA: TTimeField;
    CDSConsOSDT_FINAL: TDateField;
    CDSConsOSHR_FINAL: TTimeField;
    CDSConsOSSOLUCAO_ADOTADA: TStringField;
    CDSConsOSOBSERVACAO: TStringField;
    CDSConsOSDT_FECHAMENTO: TDateField;
    CDSConsOSHR_FECHAMENTO: TTimeField;
    CDSConsOSSTATUS: TStringField;
    CDSConsOSNOME: TStringField;
    CDSConsOSENDERECO: TStringField;
    CDSConsOSBAIRRO: TStringField;
    CDSConsOSREFERENCIA: TStringField;
    CDSConsOSTEL1: TStringField;
    CDSConsOSCELULAR: TStringField;
    CDSConsOSID_TECNICO_1: TIntegerField;
    CDSConsOSTECNICO: TStringField;
    CDSConsOSCELULAR_1: TStringField;
    CDSCadOSID_ORDEM: TIntegerField;
    CDSCadOSID_CLIENTE: TIntegerField;
    CDSCadOSID_TECNICO: TIntegerField;
    CDSCadOSTIPO: TStringField;
    CDSCadOSPROBLEMA: TStringField;
    CDSCadOSDT_ABERTURA: TDateField;
    CDSCadOSHR_ABERTURA: TTimeField;
    CDSCadOSDT_FINAL: TDateField;
    CDSCadOSHR_FINAL: TTimeField;
    CDSCadOSSOLUCAO_ADOTADA: TStringField;
    CDSCadOSOBSERVACAO: TStringField;
    CDSCadOSDT_FECHAMENTO: TDateField;
    CDSCadOSHR_FECHAMENTO: TTimeField;
    CDSCadOSSTATUS: TStringField;
    CDSConsClienteOSID_CLIENTE: TIntegerField;
    CDSConsClienteOSNOME: TStringField;
    CDSConsClienteOSENDERECO: TStringField;
    CDSConsClienteOSNUMERO: TStringField;
    CDSConsClienteOSBAIRRO: TStringField;
    CDSConsClienteOSCIDADE: TStringField;
    CDSConsClienteOSUF: TStringField;
    CDSConsClienteOSCEP: TStringField;
    CDSConsClienteOSREFERENCIA: TStringField;
    CDSConsClienteOSCPF_CNPJ: TStringField;
    CDSConsClienteOSRG_IE: TStringField;
    CDSConsClienteOSEMAIL: TStringField;
    CDSConsClienteOSVENCIMENTO: TStringField;
    CDSConsClienteOSOBSERVACAO: TStringField;
    CDSConsClienteOSTEL1: TStringField;
    CDSConsClienteOSTEL2: TStringField;
    CDSConsClienteOSCELULAR: TStringField;
    CDSConsClienteOSVALORMENSALIDADE: TFloatField;
    CDSConsClienteOSDATA_VENCIMENTO: TDateField;
    CDSConsClienteOSSTATUS: TStringField;
    CDSConsClienteOSNASCIMENTO: TDateField;
    CDSConsClienteOSDATA_INSTALACAO: TDateField;
    CDSConsClienteOSINADIMPLENCIA: TStringField;
    CDSConsClienteOSPARCELAS_GERADAS: TStringField;
    CDSSummaryOSABERTOS: TIntegerField;
    CDSSummaryOSEM_EXECUCAO: TIntegerField;
    CDSSummaryOSCONCLUIDOS: TIntegerField;
    CDSSummaryOSTOTAL: TIntegerField;
    FDConsUsuarios: TFDQuery;
    DSPConsUsuarios: TDataSetProvider;
    CDSConsUsuarios: TClientDataSet;
    FDCadUsuario: TFDQuery;
    DSPCadUsuario: TDataSetProvider;
    CDSCadUsuario: TClientDataSet;
    FDConsTecnicos: TFDQuery;
    DSPConsTecnicos: TDataSetProvider;
    CDSConsTecnicos: TClientDataSet;
    FDCadTecnico: TFDQuery;
    DSPCadTecnico: TDataSetProvider;
    CDSCadTecnico: TClientDataSet;
    CDSConsUsuariosID_USUARIO: TIntegerField;
    CDSConsUsuariosNOME: TStringField;
    CDSConsUsuariosSENHA: TStringField;
    CDSCadUsuarioID_USUARIO: TIntegerField;
    CDSCadUsuarioNOME: TStringField;
    CDSCadUsuarioSENHA: TStringField;
    CDSConsTecnicosID_TECNICO: TIntegerField;
    CDSConsTecnicosNOME: TStringField;
    CDSConsTecnicosCELULAR: TStringField;
    CDSCadTecnicoID_TECNICO: TIntegerField;
    CDSCadTecnicoNOME: TStringField;
    CDSCadTecnicoCELULAR: TStringField;
    FDClientesAtraso: TFDQuery;
    DSPClientesAtraso: TDataSetProvider;
    CDSClientesAtraso: TClientDataSet;
    CDSClientesAtrasoID_CLIENTE: TIntegerField;
    FDLogin: TFDQuery;
    DSPLogin: TDataSetProvider;
    CDSLogin: TClientDataSet;
    CDSLoginID_USUARIO: TIntegerField;
    CDSLoginNOME: TStringField;
    CDSLoginSENHA: TStringField;
    CDSReciboSTATUS: TStringField;
    FDRelParcelasPagas: TFDQuery;
    DSPRelParcelasPagas: TDataSetProvider;
    CDSRelParcelasPagas: TClientDataSet;
    CDSRelParcelasPagasNOME: TStringField;
    CDSRelParcelasPagasDATA_VENCIMENTO: TDateField;
    CDSRelParcelasPagasDATA_PAGAMENTO: TDateField;
    CDSRelParcelasPagasVALOR: TFloatField;
    CDSRelParcelasPagasSOMA: TAggregateField;
    FDConsOSID: TFDQuery;
    DSPConsOSID: TDataSetProvider;
    CDSConsOSID: TClientDataSet;
    IntegerField1: TIntegerField;
    IntegerField2: TIntegerField;
    IntegerField3: TIntegerField;
    StringField1: TStringField;
    StringField2: TStringField;
    DateField1: TDateField;
    TimeField1: TTimeField;
    DateField2: TDateField;
    TimeField2: TTimeField;
    StringField3: TStringField;
    StringField4: TStringField;
    DateField3: TDateField;
    TimeField3: TTimeField;
    StringField5: TStringField;
    StringField6: TStringField;
    StringField7: TStringField;
    StringField8: TStringField;
    StringField9: TStringField;
    StringField10: TStringField;
    StringField11: TStringField;
    IntegerField4: TIntegerField;
    StringField12: TStringField;
    StringField13: TStringField;
    FDReciboTodos: TFDQuery;
    DSPReciboTodos: TDataSetProvider;
    CDSReciboTodos: TClientDataSet;
    procedure FDConnBeforeConnect(Sender: TObject);
  private
    { Private declarations }
  public
    var ID_USUARIO : Integer;
        NOME_USUARIO : String;
  end;

var
  DM: TDM;
  DiretorioBanco : String;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses uFuncoes;

{$R *.dfm}

procedure TDM.FDConnBeforeConnect(Sender: TObject);
var Arquivo: TIniFile{uses IniFiles};
begin
   if not FileExists('CONFIG.ini') then
   begin
     Exit;
   end
   else
   begin
      FDConn.Params.LoadFromFile('CONFIG.ini');
      DiretorioBanco := FDConn.Params.Values['DATABASE'];
   end;
end;

end.
