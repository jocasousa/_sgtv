object FrmSelecionaClienteOS: TFrmSelecionaClienteOS
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  Caption = 'Selecione o Cliente para Abrir OS'
  ClientHeight = 417
  ClientWidth = 792
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Poppins'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 23
  object DBGrid1: TDBGrid
    AlignWithMargins = True
    Left = 20
    Top = 94
    Width = 752
    Height = 313
    Margins.Left = 20
    Margins.Top = 10
    Margins.Right = 20
    Margins.Bottom = 10
    Align = alClient
    DataSource = DSConsCliente
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -13
    TitleFont.Name = 'Poppins'
    TitleFont.Style = []
    OnDblClick = DBGrid1DblClick
    OnKeyDown = DBGrid1KeyDown
    Columns = <
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'ID_CLIENTE'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Poppins'
        Font.Style = [fsBold]
        Title.Alignment = taCenter
        Title.Caption = 'CODIGO'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -13
        Title.Font.Name = 'Poppins'
        Title.Font.Style = [fsBold]
        Width = 50
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOME'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Poppins'
        Font.Style = [fsBold]
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -13
        Title.Font.Name = 'Poppins'
        Title.Font.Style = [fsBold]
        Width = 50
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ENDERECO'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Poppins'
        Font.Style = [fsBold]
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -13
        Title.Font.Name = 'Poppins'
        Title.Font.Style = [fsBold]
        Width = 50
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'BAIRRO'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Poppins'
        Font.Style = [fsBold]
        Title.Alignment = taCenter
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -13
        Title.Font.Name = 'Poppins'
        Title.Font.Style = [fsBold]
        Width = 50
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'CIDADE'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Poppins'
        Font.Style = [fsBold]
        Title.Alignment = taCenter
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -13
        Title.Font.Name = 'Poppins'
        Title.Font.Style = [fsBold]
        Width = 50
        Visible = True
      end>
  end
  object Panel1: TPanel
    AlignWithMargins = True
    Left = 20
    Top = 3
    Width = 752
    Height = 78
    Margins.Left = 20
    Margins.Right = 20
    Align = alTop
    BevelOuter = bvNone
    Caption = 'Panel1'
    ShowCaption = False
    TabOrder = 1
    object GroupBox2: TGroupBox
      Left = 15
      Top = 3
      Width = 722
      Height = 74
      Caption = 'Pesquisa Cliente'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Poppins'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      object Label2: TLabel
        Left = 18
        Top = 33
        Width = 38
        Height = 23
        Caption = 'Filtro:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Poppins'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object CBFiltro: TComboBox
        Left = 62
        Top = 28
        Width = 179
        Height = 31
        TabOrder = 0
        Items.Strings = (
          'Codigo'
          'Nome'
          'Endere'#231'o'
          'Bairro'
          'Telefone'
          'Celular'
          'Status'
          'Vencimento'
          'CPF'
          'RG')
      end
      object SBPesquisar: TSearchBox
        Left = 280
        Top = 28
        Width = 409
        Height = 31
        CharCase = ecUpperCase
        TabOrder = 1
        OnInvokeSearch = SBPesquisarInvokeSearch
      end
    end
  end
  object DSConsCliente: TDataSource
    DataSet = DM.CDSConsClienteOS
    Left = 330
    Top = 173
  end
end
