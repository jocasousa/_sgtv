program SGTV;

uses
  Vcl.Forms,
  uPrincipal in 'uPrincipal.pas' {frmPrincipal},
  uConsCliente in 'uConsCliente.pas' {frmConsCliente},
  uDM in 'uDM.pas' {DM: TDataModule},
  uFuncoes in 'uFuncoes.pas',
  uCadCliente in 'uCadCliente.pas' {frmCadCliente},
  uRelConsClientes in 'uRelConsClientes.pas' {frmRelConsClientes},
  uParcelas in 'uParcelas.pas' {frmParcelas},
  uOrdemServico in 'uOrdemServico.pas' {frmOrdemServico},
  uBaixarParcela in 'uBaixarParcela.pas' {frmBaixarParcela},
  uCadOS in 'uCadOS.pas' {FrmCadOS},
  uSelecionaClienteOS in 'uSelecionaClienteOS.pas' {FrmSelecionaClienteOS},
  uConsUsuarios in 'uConsUsuarios.pas' {FrmConsUsuarios},
  uCadUsuarios in 'uCadUsuarios.pas' {FrmCadUsuarios},
  uCadTecnicos in 'uCadTecnicos.pas' {FrmCadTecnicos},
  uConsTecnicos in 'uConsTecnicos.pas' {FrmConsTecnicos},
  uLogin in 'uLogin.pas' {FrmLogin},
  uRelParcelasPagas in 'uRelParcelasPagas.pas' {FrmRelParcelasPagas},
  uConfig in 'uConfig.pas' {FrmConfig};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TDM, DM);
  Application.CreateForm(TfrmPrincipal, frmPrincipal);
  Application.CreateForm(TFrmLogin, FrmLogin);
  Application.Run;
end.
