unit uCadOS;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls, Data.DB,
  Vcl.Mask, Vcl.DBCtrls, Vcl.WinXCtrls, Vcl.Imaging.pngimage;

type
  TFrmCadOS = class(TForm)
    Panel1: TPanel;
    DSCadOS: TDataSource;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    DBEdit4: TDBEdit;
    DataSource1: TDataSource;
    Label4: TLabel;
    DBEdit5: TDBEdit;
    Label5: TLabel;
    DBEdit6: TDBEdit;
    Label6: TLabel;
    DBEdit7: TDBEdit;
    Label7: TLabel;
    DBEdit8: TDBEdit;
    Label8: TLabel;
    DBEdit9: TDBEdit;
    Label9: TLabel;
    DBEdit10: TDBEdit;
    DSConsCliente: TDataSource;
    BtnSelecionaCliente: TButton;
    GroupBox2: TGroupBox;
    CBTipo: TComboBox;
    Label1: TLabel;
    MemoProblema: TMemo;
    Label2: TLabel;
    MemoObservacao: TMemo;
    Label10: TLabel;
    Shape1: TShape;
    Label11: TLabel;
    EdtSolucaoAdotada: TEdit;
    Label12: TLabel;
    DSConsTecnicosOS: TDataSource;
    DBlookTecnico: TDBLookupComboBox;
    BtnSalvar: TImage;
    BtnCancelar: TImage;
    CBStatus: TComboBox;
    Label13: TLabel;
    procedure BtnSelecionaClienteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtnSalvarClick(Sender: TObject);
    procedure BtnCancelarClick(Sender: TObject);
  private
    { Private declarations }
  public
  Var Editando : Boolean;
    { Public declarations }
  end;

var
  FrmCadOS: TFrmCadOS;

implementation

{$R *.dfm}

uses uDM, uSelecionaClienteOS, uFuncoes, uOrdemServico;

procedure TFrmCadOS.BtnCancelarClick(Sender: TObject);
begin
  DM.CDSCadOS.Cancel;
  Close;
end;

procedure TFrmCadOS.BtnSalvarClick(Sender: TObject);
begin

 if Editando then
 begin
  DM.CDSCadOSTIPO.Value      := CBTipo.Text;
  DM.CDSCadOSPROBLEMA.Value  := MemoProblema.Text;
  DM.CDSCadOSOBSERVACAO.Value   := MemoObservacao.Text;
  DM.CDSCadOSSOLUCAO_ADOTADA.Value   := EdtSolucaoAdotada.Text;

  case CBStatus.ItemIndex of
    0:  DM.CDSCadOSSTATUS.Value       := 'A' ;
    1:  DM.CDSCadOSSTATUS.Value       := 'E' ;
    2:  DM.CDSCadOSSTATUS.Value       := 'C' ;
  end;
 end
 else
 begin

  DM.CDSCadOSID_ORDEM.Value    := uFuncoes.RetornaId('ID_ORDEM');
  DM.CDSCadOSID_CLIENTE.Value := DM.CDSConsClienteOSID_CLIENTE.Value;
  DM.CDSCadOSTIPO.Value      := CBTipo.Text;
  DM.CDSCadOSPROBLEMA.Value  := MemoProblema.Text;
  DM.CDSCadOSDT_ABERTURA.Value  := Now;
  DM.CDSCadOSHR_ABERTURA.Value  := Now;
  DM.CDSCadOSOBSERVACAO.Value   := MemoObservacao.Text;
  DM.CDSCadOSSOLUCAO_ADOTADA.Value   := EdtSolucaoAdotada.Text;

  case CBStatus.ItemIndex of
    0:  DM.CDSCadOSSTATUS.Value       := 'A' ;
    1:  DM.CDSCadOSSTATUS.Value       := 'E' ;
    2:  DM.CDSCadOSSTATUS.Value       := 'C' ;
  end;
 end;


  DM.CDSCadOS.Post;
  DM.CDSCadOS.ApplyUpdates(0);

  DM.CDSConsOS.Close;
  DM.CDSConsOS.Open;

  frmOrdemServico.PesquisaOS;

  Close;

end;

procedure TFrmCadOS.BtnSelecionaClienteClick(Sender: TObject);
begin
     try
       Application.CreateForm(TFrmSelecionaClienteOS, FrmSelecionaClienteOS);
       FrmSelecionaClienteOS.ShowModal;
     finally
       FrmSelecionaClienteOS.Free;
     end;
end;


procedure TFrmCadOS.FormCreate(Sender: TObject);
begin
  DM.CDSConsTecnicosOS.Close;
  DM.CDSConsTecnicosOS.Open;

end;

end.
