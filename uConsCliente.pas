unit uConsCliente;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.ExtCtrls, Vcl.Grids,
  Vcl.DBGrids, Vcl.StdCtrls, Vcl.Imaging.pngimage, Vcl.WinXCtrls;

type
  TfrmConsCliente = class(TForm)
    DBGrid1: TDBGrid;
    Panel1: TPanel;
    Panel2: TPanel;
    DSConsCliente: TDataSource;
    Label1: TLabel;
    Panel3: TPanel;
    Image1: TImage;
    Image2: TImage;
    GroupBox1: TGroupBox;
    SBPesquisar: TSearchBox;
    CBFiltro: TComboBox;
    Label2: TLabel;
    Image3: TImage;
    LbQntdRegistro: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure Image1Click(Sender: TObject);
    procedure Image2Click(Sender: TObject);
    procedure SBPesquisarInvokeSearch(Sender: TObject);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure Image3Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure DBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConsCliente: TfrmConsCliente;

implementation

{$R *.dfm}

uses uDM, uFuncoes, uCadCliente, uRelConsClientes;



procedure TfrmConsCliente.DBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
const
clPaleGreen = TColor($98FB98);
clPaleRed = TColor($7280FA);
clPaleYellow = TColor($00FFFF);
clPaleBlue = TColor($E6D8AD);
clPaleTeal = TColor($808000);
clBlue = TColor($FF0000);
clGray = TColor ($A9A9A9);
begin

   //ATIVOS
  if Column.Field.Dataset.FieldbyName('STATUS').AsString = 'ATIVO'
   then
   begin
       If (gdFocused in State)
	     then
     else DBGrid1.Canvas.brush.color := clPaleGreen;
              DBGrid1.DefaultDrawColumnCell(rect,DataCol,Column,State);
   end;


  //BLOQUEADOS
  if Column.Field.Dataset.FieldbyName('STATUS').AsString = 'BLOQUEADO'
   then
   begin
       If (gdFocused in State)
	     then
     else DBGrid1.Canvas.brush.color := clPaleRed;
              DBGrid1.DefaultDrawColumnCell(rect,DataCol,Column,State);
   end;

  //INATIVOS
  if Column.Field.Dataset.FieldbyName('STATUS').AsString = 'CANCELADO'
   then
   begin
       If (gdFocused in State)
	     then
     else DBGrid1.Canvas.brush.color := clGray;
              DBGrid1.DefaultDrawColumnCell(rect,DataCol,Column,State);
   end;
end;

procedure TfrmConsCliente.DBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if Key = VK_RETURN then
 begin
   try
    Application.CreateForm(TfrmCadCliente, frmCadCliente);

    DM.CDSCadCliente.Close;
    DM.CDSCadCliente.ParamByName('ID_CLIENTE').Value := DM.CDSConsClienteID_CLIENTE.Value;
    DM.CDSCadCliente.Open;

    if DM.CDSConsCliente.RecordCount > 0 then
    begin
       frmCadCliente.EdtCodigo.Text    := DM.CDSConsClienteID_CLIENTE.AsString;
       frmCadCliente.DTnascimento.Date := DM.CDSConsClienteNASCIMENTO.Value;
       frmCadCliente.DTInstala��o.Date := DM.CDSConsClienteDATA_INSTALACAO.Value;
       frmCadCliente.EdtTelefone1.Text := DM.CDSConsClienteTEL1.Value;
       frmCadCliente.EdtTelefone2.Text := DM.CDSCadClienteTEL2.Value;
       frmCadCliente.EdtCelular.Text   := DM.CDSCadClienteCELULAR.Value;
       frmCadCliente.CBStatus.Text     := DM.CDSCadClienteSTATUS.Value;
       frmCadCliente.EdtCep.Text       := DM.CDSCadClienteCEP.Value;
       frmCadCliente.CBInadimplencia.Text := DM.CDSCadClienteINADIMPLENCIA.Value;

       frmCadCliente.GroupBox1.Enabled := False;
       frmCadCliente.GroupBox2.Enabled := False;
       frmCadCliente.BtnSalvar.Visible := False;
       frmCadCliente.BtnCancelar.Visible := False;
       frmCadCliente.lbmodoVisualizacao.Visible := True;
    end;

    frmCadCliente.ShowModal;
  finally
    frmCadCliente.Free;
  end;
 end;


 if Key = VK_DELETE then
 begin
   if MessageBox(0, 'Deseja Excluir o Cliente?', '', MB_ICONQUESTION or MB_YESNO) = mrYes  then
   begin
    DM.CDSCadCliente.Close;
    DM.CDSCadCliente.ParamByName('ID_CLIENTE').Value := DM.CDSConsClienteID_CLIENTE.Value;
    DM.CDSCadCliente.Open;
    DM.CDSCadCliente.Edit;

    DM.CDSCadCliente.Delete;
    DM.CDSCadCliente.ApplyUpdates(0);

    DM.CDSConsCliente.Close;
    DM.CDSConsCliente.Open;
   end;
  end;
end;

procedure TfrmConsCliente.FormCreate(Sender: TObject);
begin
  uFuncoes.DimensionarGrid(DBGrid1);

  DM.CDSConsCliente.Close;
  DM.CDSConsCliente.Open;

  LbQntdRegistro.Caption := 'Qtd. de Registros: ' + DM.CDSConsCliente.RecordCount.ToString;
end;

procedure TfrmConsCliente.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if Key = VK_ESCAPE then
  begin
    Close;
  end;
end;

procedure TfrmConsCliente.FormResize(Sender: TObject);
begin
  uFuncoes.DimensionarGrid(DBGrid1);
end;

procedure TfrmConsCliente.Image1Click(Sender: TObject);
begin
  try
    Application.CreateForm(TfrmCadCliente, frmCadCliente);
    DM.CDSCadCliente.Close;
    DM.CDSCadCliente.Open;
    DM.CDSCadCliente.Append;
    DM.CDSCadClienteID_CLIENTE.Value := 0;
    frmCadCliente.NovoCadastro := True;
    frmCadCliente.ShowModal;
  finally
    frmCadCliente.Free;
  end;
end;

procedure TfrmConsCliente.Image2Click(Sender: TObject);
begin
  try
    Application.CreateForm(TfrmCadCliente, frmCadCliente);

    DM.CDSCadCliente.Close;
    DM.CDSCadCliente.ParamByName('ID_CLIENTE').Value := DM.CDSConsClienteID_CLIENTE.Value;
    DM.CDSCadCliente.Open;
    DM.CDSCadCliente.Edit;
    frmCadCliente.NovoCadastro := False;

    if DM.CDSConsCliente.RecordCount > 0 then
    begin
       frmCadCliente.EdtCodigo.Text    := DM.CDSConsClienteID_CLIENTE.AsString;
       frmCadCliente.DTnascimento.Date := DM.CDSConsClienteNASCIMENTO.Value;
       frmCadCliente.DTInstala��o.Date := DM.CDSConsClienteDATA_INSTALACAO.Value;
       frmCadCliente.EdtTelefone1.Text := DM.CDSConsClienteTEL1.Value;
       frmCadCliente.EdtTelefone2.Text := DM.CDSCadClienteTEL2.Value;
       frmCadCliente.EdtCelular.Text   := DM.CDSCadClienteCELULAR.Value;
       frmCadCliente.CBStatus.Text     := DM.CDSCadClienteSTATUS.Value;
       frmCadCliente.EdtCep.Text       := DM.CDSCadClienteCEP.Value;
       frmCadCliente.CBInadimplencia.Text := DM.CDSCadClienteINADIMPLENCIA.Value;
    end;

    frmCadCliente.ShowModal;
  finally
    frmCadCliente.Free;
  end;
end;

procedure TfrmConsCliente.Image3Click(Sender: TObject);
begin
  try
    Application.CreateForm(TfrmRelConsClientes, frmRelConsClientes);


    case CBFiltro.ItemIndex of
     0:
      begin
       frmRelConsClientes.RLNameRel.Caption := 'Relat�rio por Codigo: ' + SBPesquisar.Text;
      end;


     1:
      begin
       frmRelConsClientes.RLNameRel.Caption := 'Relat�rio por Nome: ' + SBPesquisar.Text;
      end;

      2:
      begin
       frmRelConsClientes.RLNameRel.Caption := 'Relat�rio por Endere�o: ' + SBPesquisar.Text;
      end;

      3:
      begin
       frmRelConsClientes.RLNameRel.Caption := 'Relat�rio por Bairro: ' + SBPesquisar.Text;
      end;


      4:
      begin
       frmRelConsClientes.RLNameRel.Caption := 'Relat�rio por Telefone: ' + SBPesquisar.Text;
      end;


      5:
      begin
       frmRelConsClientes.RLNameRel.Caption := 'Relat�rio por Celular: ' + SBPesquisar.Text;
      end;

      6:
      begin
       frmRelConsClientes.RLNameRel.Caption := 'Relat�rio por Status: ' + SBPesquisar.Text;
      end;

      7:
      begin
       frmRelConsClientes.RLNameRel.Caption := 'Relat�rio por Dia Vencimento: ' + SBPesquisar.Text;
      end;

      else
      begin
         frmRelConsClientes.RLNameRel.Caption := 'Todos os Clientes';
      end;
    end;

    frmRelConsClientes.RLReport1.Preview();
  finally
   frmRelConsClientes.Free;
  end;
end;

procedure TfrmConsCliente.SBPesquisarInvokeSearch(Sender: TObject);
begin

 {
  Codigo
  Nome
  Endere�o
  Bairro
  Telefone
  Celular
  Status
  Vencimento
  CPF
  RG
 }


  case CBFiltro.ItemIndex of

    0:
    begin
     DM.CDSConsCliente.Close;
     DM.CDSConsCliente.Open;

     DM.CDSConsCliente.Filtered := False;
     DM.CDSConsCliente.Filter   := 'ID_CLIENTE='  + QuotedStr(SBPesquisar.Text);
     DM.CDSConsCliente.Filtered := True;

     if DM.CDSConsCliente.RecordCount = 0 then
        ShowMessage('N�o foi possivel encontrar nada com essa pesquisa');

    end;


   1:
    begin
     DM.CDSConsCliente.Close;
     DM.CDSConsCliente.Open;

     DM.CDSConsCliente.Filtered := False;
     DM.CDSConsCliente.Filter   := 'NOME LIKE '  + QuotedStr(SBPesquisar.Text + '%');
     DM.CDSConsCliente.Filtered := True;

     if DM.CDSConsCliente.RecordCount = 0 then
        ShowMessage('N�o foi possivel encontrar nada com essa pesquisa');
    end;

    2:
    begin
     DM.CDSConsCliente.Close;
     DM.CDSConsCliente.Open;

     DM.CDSConsCliente.Filtered := False;
     DM.CDSConsCliente.Filter   := 'ENDERECO LIKE '  + QuotedStr(SBPesquisar.Text + '%');
     DM.CDSConsCliente.Filtered := True;

     if DM.CDSConsCliente.RecordCount = 0 then
        ShowMessage('N�o foi possivel encontrar nada com essa pesquisa');
    end;

    3:
    begin
     DM.CDSConsCliente.Close;
     DM.CDSConsCliente.Open;

     DM.CDSConsCliente.Filtered := False;
     DM.CDSConsCliente.Filter   := 'BAIRRO LIKE '  + QuotedStr(SBPesquisar.Text + '%');
     DM.CDSConsCliente.Filtered := True;

     if DM.CDSConsCliente.RecordCount = 0 then
        ShowMessage('N�o foi possivel encontrar nada com essa pesquisa');
    end;


    4:
    begin
     DM.CDSConsCliente.Close;
     DM.CDSConsCliente.Open;

     DM.CDSConsCliente.Filtered := False;
     DM.CDSConsCliente.Filter   := 'TELEFONE LIKE '  + QuotedStr(SBPesquisar.Text + '%');
     DM.CDSConsCliente.Filtered := True;

     if DM.CDSConsCliente.RecordCount = 0 then
        ShowMessage('N�o foi possivel encontrar nada com essa pesquisa');
    end;


    5:
    begin
     DM.CDSConsCliente.Close;
     DM.CDSConsCliente.Open;

     DM.CDSConsCliente.Filtered := False;
     DM.CDSConsCliente.Filter   := 'CELULAR LIKE '  + QuotedStr(SBPesquisar.Text + '%');
     DM.CDSConsCliente.Filtered := True;

     if DM.CDSConsCliente.RecordCount = 0 then
        ShowMessage('N�o foi possivel encontrar nada com essa pesquisa');
    end;

    6:
    begin
     DM.CDSConsCliente.Close;
     DM.CDSConsCliente.Open;

     DM.CDSConsCliente.Filtered := False;
     DM.CDSConsCliente.Filter   := 'STATUS='  + QuotedStr(SBPesquisar.Text);
     DM.CDSConsCliente.Filtered := True;

     if DM.CDSConsCliente.RecordCount = 0 then
        ShowMessage('N�o foi possivel encontrar nada com essa pesquisa');
    end;

    7:
    begin
     DM.CDSConsCliente.Close;
     DM.CDSConsCliente.Open;

     DM.CDSConsCliente.Filtered := False;
     DM.CDSConsCliente.Filter   := 'VENCIMENTO='  + QuotedStr(SBPesquisar.Text);
     DM.CDSConsCliente.Filtered := True;

     if DM.CDSConsCliente.RecordCount = 0 then
        ShowMessage('N�o foi possivel encontrar nada com essa pesquisa');
    end;

    8:
    begin
     DM.CDSConsCliente.Close;
     DM.CDSConsCliente.Open;

     DM.CDSConsCliente.Filtered := False;
     DM.CDSConsCliente.Filter   := 'CPF_CNPJ =' + QuotedStr(SBPesquisar.Text);
     DM.CDSConsCliente.Filtered := True;

     if DM.CDSConsCliente.RecordCount = 0 then
        ShowMessage('N�o foi possivel encontrar nada com essa pesquisa');
    end;


    9:
    begin
     DM.CDSConsCliente.Close;
     DM.CDSConsCliente.Open;

     DM.CDSConsCliente.Filtered := False;
     DM.CDSConsCliente.Filter   := 'RG_IE='  + QuotedStr(SBPesquisar.Text);
     DM.CDSConsCliente.Filtered := True;

     if DM.CDSConsCliente.RecordCount = 0 then
        ShowMessage('N�o foi possivel encontrar nada com essa pesquisa');
    end;

    else
    begin
       ShowMessage('Selecione o Filtro antes da Pesquisa.');
    end;
  end;

  LbQntdRegistro.Caption := 'Qtd. de Registros: ' + DM.CDSConsCliente.RecordCount.ToString;
end;

end.
