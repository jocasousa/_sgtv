unit uCadUsuarios;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.Imaging.pngimage,
  Vcl.StdCtrls, Vcl.Mask, Vcl.DBCtrls, Data.DB;

type
  TFrmCadUsuarios = class(TForm)
    Panel1: TPanel;
    DataSource1: TDataSource;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label2: TLabel;
    BtnSalvar: TImage;
    BtnCancelar: TImage;
    procedure BtnSalvarClick(Sender: TObject);
    procedure BtnCancelarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmCadUsuarios: TFrmCadUsuarios;

implementation

{$R *.dfm}

uses uDM, uFuncoes;

procedure TFrmCadUsuarios.BtnCancelarClick(Sender: TObject);
begin
  DM.CDSCadUsuario.Cancel;
  Close;
end;

procedure TFrmCadUsuarios.BtnSalvarClick(Sender: TObject);
begin

  if DM.CDSCadUsuarioID_USUARIO.Value = 0 then
  begin
    DM.CDSCadUsuarioID_USUARIO.Value := uFuncoes.RetornaId('ID_USUARIO');
  end;

  DM.CDSCadUsuario.Post;
  DM.CDSCadUsuario.ApplyUpdates(0);

  DM.CDSConsUsuarios.Close;
  DM.CDSConsUsuarios.Open;

  Close;
end;

procedure TFrmCadUsuarios.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if key = VK_ESCAPE then
 begin
   DM.CDSCadUsuario.Cancel;
   Close;
 end;
end;

end.
